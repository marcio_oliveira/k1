<html>
    <style>* {margin:0;padding:0;}body {background:#f0f0f0; font-size:14px;font-family:Arial,Helvetica,sans-serif}p,form{padding:5px 20px}input{padding:5px 10}input.error{background:#fee;}button{background:#ccc;padding:5px 10px;color:#fff;margin-top:10px}.field{padding:10px 0}}</style>
    <body>
<?php 

    function human_filesize($path, $decimals = 0) {
        $bytes = filesize($path);
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }

    $filenameExists = isset($_POST['filename']);
    $foldernameExists = isset($_POST['foldername']);
    $filenameIsSet = $filenameExists && $_POST['filename']!='';
    $foldernameIsSet = $foldernameExists && $_POST['foldername']!='';

    if(!$filenameIsSet || !$foldernameIsSet) {?>
    
        <form name="form" method="POST">
            <div class="field">
                <label for="filename">Please give a filename to the .tar.gz file</label>
                <br/>
                <input name="filename" value="<?php if($filenameIsSet) echo $_POST['filename'] ?>" <?php if($filenameExists && !$filenameIsSet) { echo 'class="error"';} ?> />
            </div>
            <div class="field">
                <label for="foldername">Type the foldername to be in .tar.gz</label>
                <br/>
                <input name="foldername" value="<?php if($foldernameIsSet) echo $_POST['foldername'] ?>" <?php if($foldernameExists && !$foldernameIsSet) { echo 'class="error"';} ?> />
            </div>
            <button>TAR it</button>
        </form>
    <?php } else {

    
        foreach($_POST as $key => $value) {
            $$key = $value;
        }

        $command = "tar -cvf {$filename}.tar.gz {$foldername}";
        exec($command);
        $link = $_SERVER['REQUEST_URI'];
        $scriptName = explode('/',$link);
        $scriptName = array_pop($scriptName);
        $tarName = "$filename.tar.gz";
        $link = str_replace($scriptName,$tarName,$link);
        $abs = __DIR__."/".$tarName;
    ?>
        <form>
            <h2>Download</h2><br/>
            <p>
                <a href="<?php echo $link ?>"><?php echo $tarName ?></a> (<?php echo human_filesize($abs) ?>)
            </p>
        </form>
    <?php 
    }
    ?>
    </body>
</html>