function is_et_fb_gtm_dfa() {
    if( document.getElementById( 'et-fb-app' ) ) {
		return true;
    }
    return false;
}
	
/**
 * Check icon to see if it's a FA icon.
 *
 * @since 1.0.0
 */
function gtm_dfa_check( string ) {
	if( ! string ) {
		return false;
	}
		
	var checkpoint = string.substring(0, 3);
	if( checkpoint === 'fa-' ) {
		return true;
	} else {
		return false;
	}
}
	
/**
 * Add Class
 *
 * @since 1.2.0
 */
var hasClass = function ( elem, className ) {
    return new RegExp( ' ' + className + ' ' ).test( ' ' + elem.className + ' ' );
};
	
/**
 * If element has class
 *
 * @since 1.2.0
 */
var addClass = function ( elem, className ) {
    if ( !hasClass( elem, className ) ) {
    	elem.className += ' ' + className;
    }
};
		
/**
 * Parse icon data and display icons on the front end.
 *
 * @since 1.2.0
 */
function trigger_icons_gtm_dfa() {
		
	var icon_modules = document.body.querySelectorAll('.et_pb_button,.et-pb-icon,.et_overlay,.et_pb_extra_overlay,.et_pb_dmb_breadcrumbs li,.et_pb_dmb_breadcrumbs a,.dwd-slider-fullscreen button.slick-arrow');
	var i = 0;
	
	for( i = 0; i < icon_modules.length; i++ ) {
		
		var module = icon_modules[i];
        
        var icon_parts = '';
															
		if ( module.classList.contains( 'et-pb-icon' ) ) {
								
			var checkpoint = gtm_dfa_check( module.innerHTML );
			if( checkpoint ) {
				addClass( module, 'divi_fontawesome' );
				module.innerHTML =  module.innerHTML.substr(3);
			}
		
		} else {
			
			var checkpoint = gtm_dfa_check( module.getAttribute( 'data-icon') );
		
			if( checkpoint ) {
				module.setAttribute( 'data-icon', module.getAttribute( 'data-icon').substr(3) );
				addClass( module, 'divi_fontawesome' );
			}
		}
	}
}

/**
* Update icons in the FB list
*
* @since    1.2.0
*/
function swap_icon_list_gtm_dfa() {
			
	var icon_list = document.getElementById( 'et-fb-font_icon' ).childNodes;
			
	for( var i = 0; i < icon_list.length; i++ ) {
				
		var icon_item = icon_list[i];
		var checkpoint = gtm_dfa_check( icon_item.dataset.icon );
					
		if( checkpoint ) {
			icon_item.setAttribute( 'data-icon', icon_item.dataset.icon.substr(3) );
			addClass( icon_item, 'divi_fontawesome' );
		}
	}
}
	
/**
 * Listen for the FB to activate
 *
 * @since    1.2.0
 */
function listen_gtm_dfa() {
	var root = document.documentElement;
	var et_fb_observer = new MutationObserver(function(mutations) {
   		mutations.forEach(function() {
        	trigger_icons_gtm_dfa();
        	et_fb_observer.disconnect();
    	});
	});
	var config = { attributes: true, characterData: true };
	et_fb_observer.observe(root, config);
}
	
/**
 * Listen for Changes in app and trigger appropriate icons.
 *
 * @since 1.2.0
 */
var et_fb_gtm_dfa = is_et_fb_gtm_dfa();
		
if( et_fb_gtm_dfa ) {	
			
	var app_id_gtm_dfa = 'et-fb-app';
	var fb_icon_list_gtm_dfa = 'et-fb-font_icon';
			
	// LISTEN FOR OVERALL CHANGES IN THE APP
	document.getElementById( app_id_gtm_dfa ).addEventListener('DOMSubtreeModified', function () {
  				
  		// PARSE ICON DATA
  		trigger_icons_gtm_dfa();
  				
  		// LISTEN FOR ICON LIST
  		if( document.getElementById( fb_icon_list_gtm_dfa ) ) {
  		swap_icon_list_gtm_dfa();
  		}
	}, false);

	listen_gtm_dfa();
} else {
	trigger_icons_gtm_dfa();
}

var fb_modal_observer = new MutationObserver( function( mutations ) {
    mutations.forEach( function( mutation ) {
        
        if ( mutation.addedNodes && mutation.addedNodes.length > 0) {
            // element added to DOM            
            var checkForClass = [].some.call(mutation.addedNodes, function(el) {
                return el.classList.contains('et-fb-modal__module-settings')
            });
            if ( checkForClass ) {
                swap_icon_list_gtm_dfa();
            }
        }
    });
});

var config = {
    attributes: true,
    childList: true,
    characterData: true
};

fb_modal_observer.observe(document.body, config);