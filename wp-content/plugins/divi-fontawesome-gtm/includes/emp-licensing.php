<?php

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
	require_once( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

function gtm_dfa_plugin_updater() {
		
	// retrieve our license key from the DB
	$license_key = trim( get_option( 'divi_fontawesome_gtm_license_key' ) );

	// setup the updater
	$edd_updater = new EDD_SL_Plugin_Updater( GTM_DFA_STORE_URL, __FILE__, array(
			'version' 	=> GTM_DFA_VERSION, 				// current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => GTM_DFA_ITEM_NAME, 	// name of this plugin
			'author' 	=> GTM_DFA_AUTHOR,  // author of this plugin
			'beta'		=> false
		)
	);

}
add_action( 'admin_init', 'gtm_dfa_plugin_updater', 0 );

function gtm_dfa_license_fields() {

	if ( isset( $_POST['gtm_dfa_update_licensing'])) {
						
		$old = get_option( 'divi_fontawesome_gtm_license_key' );
		$new = $_POST['divi_fontawesome_gtm_license_key'];
		
		update_option('divi_fontawesome_gtm_license_key', $new);
		
		if( $old && $old != $new ) {
			delete_option( 'divi_fontawesome_gtm_license_status' ); // new license has been entered, so must reactivate
		}
		
		gtm_dfa_activate_license( true );
		
	}
	
	$license = get_option( 'divi_fontawesome_gtm_license_key' );
	$status  = get_option( 'divi_fontawesome_gtm_license_status' );
	$data  = get_option( 'divi_fontawesome_gtm_license_data' );
	
	//echo '<pre>';
	//print_r($data);
	//echo '</pre>';
	
	// echo gtm_dfa_box_start('Plugin Licensing');
	
	if ( $status !== false && $status == 'valid' ) :
		$license_icon = '<th scope="row" valign="top" class="dashicons-before dashicons-unlock gtm_dfa_settings_label_icon gtm_dfa_settings_label_icon--unlocked">' . __('License Key') . '</th>';
	else :
		$license_icon = '<th scope="row" valign="top" class="dashicons-before dashicons-lock gtm_dfa_settings_label_icon gtm_dfa_settings_label_icon--locked">' . __('License Key') . '</th>';
	endif;
	
	echo '<table class="widefat">
				<tbody>
					<tr valign="top">' .
						$license_icon .
						'<td>
							<input id="divi_fontawesome_gtm_license_key" name="divi_fontawesome_gtm_license_key" type="text" class="regular-text" value="' . esc_attr__( $license ) . '" />
						</td>
					</tr>';
					
					if( false !== $license ) {
						echo '<tr valign="top">
							<th scope="row" valign="top">' . __('License Status') . '</th>
							<td>';
							
							if ( $status !== false && $status == 'valid' ) {
									echo '<p><span style="color:#1abc9c;">' . __('License Active') . '</span></p>';
									echo '<p>Expiry: ' . $data->expires . '</p>';
							} else {
								echo '<span style="color:#e74c3c;">' . __('License NOT Active') . '</span>';
							}
							
							echo '</td>
						</tr>';
					}
				echo '</tbody>
			</table>
			
			<p><input type="submit" name="gtm_dfa_update_licensing" class="button-primary" value="Save License Key" /></p>';
			
	// echo gtm_dfa_box_end();
	
}

function gtm_dfa_has_license() {
	$status  = get_option( 'divi_fontawesome_gtm_license_status', false );
	
	return ($status && $status == 'valid');
}

function gtm_dfa_activate_license( $bypass = false ) {

	if( isset( $_POST['gtm_dfa_activate'] ) || $bypass ) {

		$license = trim( get_option( 'divi_fontawesome_gtm_license_key' ) );

		$api_params = array(
			'edd_action' => 'activate_license',
			'license'    => $license,
			'item_name'  => urlencode( GTM_DFA_ITEM_NAME ), // the name of our product in EDD
			'url'        => home_url()
		);

		// Call the custom API.
		$response = wp_remote_post( GTM_DFA_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

		// make sure the response came back okay
		if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {

			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = __( 'An error occurred, please try again.' );
			}

		} else {

			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			if ( false === $license_data->success ) {

				switch( $license_data->error ) {

					case 'expired' :

						$message = sprintf(
							__( 'Your license key expired on %s.' ),
							date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
						);
						break;

					case 'revoked' :

						$message = __( 'Your license key has been disabled.' );
						break;

					case 'missing' :

						$message = __( 'Invalid license.' );
						break;

					case 'invalid' :
					case 'site_inactive' :

						$message = __( 'Your license is not active for this URL.' );
						break;

					case 'item_name_mismatch' :

						$message = sprintf( __( 'This appears to be an invalid license key for %s.' ), GTM_DFA_ITEM_NAME );
						break;

					case 'no_activations_left':

						$message = __( 'Your license key has reached its activation limit.' );
						break;

					default :

						$message = __( 'An error occurred, please try again.' );
						break;
				}

			}

		}

		// Check if anything passed on a message constituting a failure
		if ( ! empty( $message ) ) {
			echo '<p class="updated fade">' . $message . '</p>';
		} else {
			update_option( 'divi_fontawesome_gtm_license_status', $license_data->license );
			update_option( 'divi_fontawesome_gtm_license_data', $license_data );
		}

	}
}