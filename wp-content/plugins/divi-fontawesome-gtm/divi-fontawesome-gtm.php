<?php

/**
 * @link              https://greentreemediallc.com
 * @since             1.1.0
 * @wordpress-plugin
 * Plugin Name:       Divi Font Awesome
 * Description:       Easily load the iconic Font Awesome directly into the Divi Builder. Seriously.
 * Plugin URI:	      http://alexbrinkman.org/divi-font-awesome/
 * Version:           1.2.1
 * Author:            Alex Brinkman
 * Author URI:        https://greentreemediallc.com
 * Text Domain:       divi-fontawesome-gtm
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) :
	die;
endif;

require_once( plugin_dir_path( __FILE__ ) . 'includes/emp-licensing.php' );

define( 'GTM_DFA_VERSION', '1.2.1' );
define( 'GTM_DFA_AUTHOR', 'Alex Brinkman' );
define( 'GTM_DFA_OPTIONS_NAME', 'divi_fontawesome_gtm_settings' );
define( 'GTM_DFA_PLUGIN_SLUG', 'divi-fontawesome-gtm' );
define( 'GTM_DFA_STORE_URL', 'https://elegantmarketplace.com' );
define( 'GTM_DFA_ITEM_NAME', 'Divi Font Awesome' );
define( 'GTM_DFA_SETTINGS', 'divi_fontawesome_gtm_settings' );

add_action( 'admin_init', 'gtm_dfa_setup_sections' );
add_action( 'admin_init', 'gtm_dfa_setup_fields' );
add_action( 'admin_menu', 'gtm_dfa_admin_menu' );

add_action( 'wp_enqueue_scripts', 'gtm_dfa_public_script' );
add_action( 'wp_enqueue_scripts', 'gtm_dfa_fontawesome' );

add_action( 'admin_enqueue_scripts', 'gtm_dfa_fontawesome' );

add_filter( 'plugin_action_links', 'gtm_dfa_add_action_plugin', 10, 5 );

/**
 * Filter plugin action links.
 *
 * @since    1.0.0
 */
function gtm_dfa_add_action_plugin( $actions, $plugin_file ) {
	static $plugin;

	if ( ! isset( $plugin ) )
		$plugin = plugin_basename(__FILE__);
	
	if ($plugin == $plugin_file) :

		$settings = array('settings' => '<a href="options-general.php?page=' . GTM_DFA_SETTINGS . '">' . __('Settings', 'General') . '</a>');
		$site_link = array('support' => '<a href="https://alexbrinkman.org/product-support/" target="_blank">Support</a>');
		
    	$actions = array_merge($settings, $actions);
		$actions = array_merge($site_link, $actions);
			
	endif;
		
	return $actions;
}

/**
 * Register the admin menu.
 *
 * @since    1.0.0
 */
function gtm_dfa_admin_menu() {
	add_submenu_page(
		'options-general.php',		// parent slug
		'Divi Font Awesome',	// page title
		'Divi Font Awesome',	// menu title
		'manage_options',			//capability
		GTM_DFA_SETTINGS,			// slug
		'gtm_dfa_settings_page' 		// callback 
	);
}

function gtm_dfa_settings_page() {
	
    settings_errors( 'settings_messages' ); ?>
	    
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<form method="post">
            		<?php gtm_dfa_license_fields( GTM_DFA_SETTINGS ); ?>
        		</form>
        
				<form method="post" action="options.php" class="gtm_dfa_settings">
				<?php
            		settings_fields( GTM_DFA_SETTINGS );    
            		do_settings_sections( GTM_DFA_SETTINGS );
            		submit_button();
        		?>
				</form>
				<p class="gtm_dfa_credits">Built with <span class="dashicons dashicons-heart"></span> by Alex Brinkman over at Green Tree Media. Have a great day!</p>
				<p class="gtm_dfa_credits">If you made something cool with this plugin, <a href="greentreemediallc.com/twitter" target="_blank">tweet me</a> and let me know!</p>
			</div>
		</div>
	</div>
	<?php
}

function gtm_dfa_setup_sections() {
	add_settings_section( 'divi_fontawesome_settings', '', 'gtm_dfa_section_callback', GTM_DFA_SETTINGS );
}

function gtm_dfa_section_callback( $arguments ) {
	switch( $arguments['id'] ) :
		case 'divi_fontawesome_settings':
			echo '<p>You\'re 2 short steps away from using Font Awesome 
			directly in your Divi Builder! This plugin uses the 
			Font Awesome CDN, which will help your website load the iconic 
			Font Awesome quickly and reliably.</p>
			
			<p>You can get a FREE 
			<a href="https://cdn.fontawesome.com/" target="_blank">Font Awesome 
			CDN account here</a> to get your embed code.</p>';
		break;
	endswitch;
}
    
function gtm_dfa_setup_fields() {
        
    // Our main setting we'll be saving our settings under.
    register_setting( GTM_DFA_SETTINGS, GTM_DFA_OPTIONS_NAME );
        
    $settings = get_option( GTM_DFA_OPTIONS_NAME );
        
    // Set variables 
    $fa_embed_code_id 		= isset( $settings['fa_embed_code_id'] ) ? $settings['fa_embed_code_id'] : '';
    $fa_embed_code_format 	= isset( $settings['fa_embed_code_format'] ) ? $settings['fa_embed_code_format'] : '';
             
    $fields = array(
		array(
	    	'uid' => 'fa_embed_code_id',
		    'label' => '<span class="dashicons-before dashicons-cloud gtm_dfa_settings_label_icon">CDN Embed Code ID</span>',
		    'section' => 'divi_fontawesome_settings',
		    'type' => 'text',
		    'options' => false,
		    'placeholder' => 'CDN Embed Code ID',
		    'helper' => '',
		    'supplemental' => '',
		    'default' => $fa_embed_code_id
	    ),
	    array(
	    	'uid' => 'fa_embed_code_format',
		    'label' => '<span class="dashicons-before dashicons-editor-code gtm_dfa_settings_label_icon">Format</span>',
		    'section' => 'divi_fontawesome_settings',
		    'type' => 'select',
		    'options' => array(
		    	'js' => 'JavaScript',
		    	'css' => 'CSS'
		    ),
		    'placeholder' => '',
		    'helper' => '',
		    'supplemental' => 'We\'re happy to serve it up however you\'d prefer.',
		    'default' => $fa_embed_code_format
	    )
    );
	   
	foreach( $fields as $field ) :
	        
	    add_settings_field(
	        $field['uid'],
	        $field['label'],          
	        'gtm_dfa_field_callback',
	        GTM_DFA_SETTINGS,
	        $field['section'],
	        $field
	    );
	        
	endforeach;
    
}

function gtm_dfa_field_callback( $arguments ) {
	        
    $value = get_option( $arguments['uid'] );
        
    if( ! $value ) : // If no value exists
        $value = $arguments['default']; // Set to our default
    endif;

	// Check which type of field we want
    switch( $arguments['type'] ) :
	        
	    case 'text': // If it is a text field
		    printf( '<input name="' . GTM_DFA_OPTIONS_NAME . '[%1$s]" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
		break;
		    
		case 'number': // If it is a number field
		    printf( '<input name="' . GTM_DFA_OPTIONS_NAME . '[%1$s]" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
		break;
	        
	    case 'textarea': // If it is a textarea
		    printf( '<textarea name="' . GTM_DFA_OPTIONS_NAME . '[%1$s]" id="%1$s" placeholder="%2$s" rows="5" cols="50">%3$s</textarea>', $arguments['uid'], $arguments['placeholder'], $value );
		break;
	        
	    case 'select': // If it is a select dropdown
		        
		    if( ! empty ( $arguments['options'] ) && is_array( $arguments['options'] ) ) :
			        
			    $options_markup = '';
			        
			    foreach( $arguments['options'] as $key => $label ) :
				    $options_markup .= sprintf( '<option value="%s" %s>%s</option>', $key, selected( $value, $key, false ), $label );
			    endforeach;
			        
			    printf( '<select name="' . GTM_DFA_OPTIONS_NAME . '[%1$s]" id="%1$s">%2$s</select>', $arguments['uid'], $options_markup );
		       
		    endif;
		        
		break;
		    
    endswitch;

	// If there is help text
    if( $helper = $arguments['helper'] ) :
        printf( '<span class="helper"> %s</span>', $helper );
    endif;

	// If there is supplemental text
    if( $supplimental = $arguments['supplemental'] ) :
        printf( '<p class="description">%s</p>', $supplimental );
    endif;
    
}

function gtm_dfa_fontawesome() {
	$settings = get_option( GTM_DFA_OPTIONS_NAME );
	$fa_embed_code_id 		= isset( $settings['fa_embed_code_id'] ) ? $settings['fa_embed_code_id'] : '';
	$fa_embed_code_format 	= isset( $settings['fa_embed_code_format'] ) ? $settings['fa_embed_code_format'] : 'JS';
	
	if( ! $fa_embed_code_id ) :
		return;
	endif;
	
	$fa_url = 'https://use.fontawesome.com/' . $fa_embed_code_id;
	
	// Load the correct format from the CDN
	if( $fa_embed_code_format === 'css' ) :
		wp_enqueue_style( GTM_DFA_PLUGIN_SLUG, $fa_url . '.css', array(), GTM_DFA_VERSION, 'all' );
	else :
		wp_enqueue_script( GTM_DFA_PLUGIN_SLUG, $fa_url . '.js', array(), GTM_DFA_VERSION, false );
	endif;
	
	// Load our custom stylesheet
	wp_enqueue_style( GTM_DFA_PLUGIN_SLUG . '-custom',  plugin_dir_url( __FILE__ ) . 'assets/' . GTM_DFA_PLUGIN_SLUG . '.css', array(), GTM_DFA_VERSION, 'all' );
}

function gtm_dfa_public_script() {
	wp_enqueue_script( GTM_DFA_PLUGIN_SLUG . '-custom',  plugin_dir_url( __FILE__ ) . 'assets/' . GTM_DFA_PLUGIN_SLUG . '.js', array(), GTM_DFA_VERSION, true );
}
		
/**
 * Add Font Awesome icons to Divi.
 *
 * @since    1.0.0
 */
function fontawesome_icons( $icons ) {
	
	include( plugin_dir_path( __FILE__ ) . 'assets/fontawesome.php' );
		
	foreach( $font_awesome as $icon ) :
		$icons[] = $icon;
	endforeach;
	
    return $icons;
}
add_filter('et_pb_font_icon_symbols', 'fontawesome_icons');

/**
 * Copy of Divi's et_pb_get_font_icon_list_items() function 
 * with the addition of the filter for the output so that we 
 * (and others) can apply their own filters if they wish.
 *
 * @since    1.0.0
 * @since	 1.1.0 Added filter and seperated custom logic from original function
 */
if ( ! function_exists( 'et_pb_get_font_icon_list_items' ) ) :
function et_pb_get_font_icon_list_items() {
	$output = '';

	$symbols = et_pb_get_font_icon_symbols();

	foreach ( $symbols as $symbol ) {
		$output .= sprintf( '<li data-icon=\'%1$s\'></li>', esc_attr( $symbol ) );
	}
	
	// This is the only alteration to this function.
	$output = apply_filters( 'gtm_dfa_icon_list_items', $output );
	
	return $output;
}
endif;

/**
 * Filter to adjust how the icons are output in the Divi Builder.
 *
 * @since    1.1.0
 */
function gtm_dfa_filter_icon_list( $data ) {
	
	$output = '';
		
	$symbols = trim( str_replace( "<li data-icon='","", $data ) );
	$symbols = explode( "'></li>", $symbols );
	$symbols = array_filter( $symbols );
	
	foreach ( $symbols as $symbol ) {
		
		$fa_check = substr( $symbol, 0, 3 );
						
		if( $fa_check === 'fa-' ) :
			
			$symbol_parts = explode( '-', $symbol );
			$output .= sprintf( '<li data-icon=\'%1$s\' class="fa fa-fw fa-divi-fontawesome-builder"></li>', esc_attr( $symbol_parts[1] ) );
		else :
			$output .= sprintf( '<li data-icon=\'%1$s\'></li>', esc_attr( $symbol ) );
		endif;
	}
	
	return $output;

}
add_filter('gtm_dfa_icon_list_items', 'gtm_dfa_filter_icon_list');