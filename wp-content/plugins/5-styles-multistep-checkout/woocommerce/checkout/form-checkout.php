<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

$tabs = array();
$settings = multisteps_get_settings();

if( !is_user_logged_in() && $settings["multistep_checkout_login"] == "yes"){
   $tabs["checkout-tab-login"] = __($settings["multistep_checkout_login_text"],"woocommerce");
}
if( $settings["multistep_checkout_coupon"] == "yes"){
    $tabs["checkout-tab-coupon"] = __($settings["multistep_checkout_coupon_text"],"woocommerce");
}
$tabs["checkout-tab-billing"]=__($settings["multistep_checkout_billing_text"],"woocommerce");
$tabs["checkout-tab-shipping"]=__($settings["multistep_checkout_shipping_text"],"woocommerce");
$tabs["checkout-tab-payment"]=__($settings["multistep_checkout_payment_text"],"woocommerce");
$key_active ="";
?>
<div class="container-checkout-steps container-checkout-steps-<?php echo $settings["multistep_checkout_style"] ?>">
    <div class="container-multistep-header">
        <ul class="checkout-display-steps-container checkout-display-steps-container-<?php echo $settings["multistep_checkout_style"] ?>">
                <?php
                $i=1;
                foreach( $tabs as $key=>$value):?>
            	<li class="<?php if( $i== 1){echo "active"; $key_active = $key; } ?> checkout-steps-<?php echo $i ?>" data-i="<?php echo $i ?>" data-tab=".<?php echo $key ?>">
            		<?php _e($value,"woocommerece"); ?>
            	</li>
                <?php
                $i++;
                 endforeach; ?>
        </ul>
    </div>
    <div class="container-body-tab">
        <?php
        if( isset($tabs["checkout-tab-login"]) ) :
         ?>
        <div class="checkout-tab checkout-tab-login">
            <?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>
            <div class="multistep-nav">
                <div class="multistep-nav-left">
                </div>
                <div class="multistep-nav-right">
                    <a href="#" class="multistep-checkout-next"><?php _e($settings["multistep_checkout_button_acount"],"woocommerce");  ?></a>
                </div>
            </div>
        </div>
        <?php endif;

        if( isset($tabs["checkout-tab-coupon"]) ) :
         ?>
        <div class="checkout-tab checkout-tab-coupon <?php if( $key_active != "checkout-tab-coupon") {echo "hidden";} ?>">
            <?php do_action( 'woocommerce_coupon_checkout_form', $checkout ); ?>
            <div class="multistep-nav">
                <div class="multistep-nav-left">
                    <?php if( $key_active == "checkout-tab-login") : ?>
                    <a href="#" class="multistep-checkout-prev"><?php _e($settings["multistep_checkout_button_prev"],"woocommerce");  ?></a>
                    <?php endif; ?>
                </div>
                <div class="multistep-nav-right">
                    <a href="#" class="multistep-checkout-next"><?php _e($settings["multistep_checkout_button_next"],"woocommerce");  ?></a>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
            <div class="checkout-tab checkout-tab-billing <?php if( $key_active != "checkout-tab-billing") {echo "hidden";} ?>">
                <?php do_action( 'woocommerce_checkout_billing' ); ?>
                <div class="multistep-nav">
                <div class="multistep-nav-left">
                    <?php if( $key_active == "checkout-tab-coupon") : ?>
                    <a href="#" class="multistep-checkout-prev"><?php _e($settings["multistep_checkout_button_prev"],"woocommerce");  ?></a>
                    <?php endif; ?>
                </div>
                <div class="multistep-nav-right">
                    <a href="#" class="multistep-checkout-next"><?php _e($settings["multistep_checkout_button_next"],"woocommerce");  ?></a>
                </div>
            </div>
            </div>
            <div class="checkout-tab checkout-tab-shipping hidden">
                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
                <?php do_action( 'woocommerce_checkout_shipping' ); ?>
                <div class="multistep-nav">
                    <div class="multistep-nav-left">
                        <a href="#" class="multistep-checkout-prev"><?php _e($settings["multistep_checkout_button_prev"],"woocommerce");  ?></a>
                    </div>
                    <div class="multistep-nav-right">
                        <a href="#" class="multistep-checkout-next"><?php _e($settings["multistep_checkout_button_next"],"woocommerce");  ?></a>
                    </div>
                </div>
            </div>
            <div class="checkout-tab checkout-tab-payment hidden">
                <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
                <h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

            	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

            	<div id="order_review" class="woocommerce-checkout-review-order">
            		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
                    <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
            	</div>
                <div class="multistep-nav">
                    <div class="multistep-nav-left">
                        <a href="#" class="multistep-checkout-prev"><?php _e($settings["multistep_checkout_button_prev"],"woocommerce");  ?></a>
                    </div>
                    <div class="multistep-nav-right">

                    </div>
                </div>
            </div>
        </form>
        <?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
    </div>
</div>