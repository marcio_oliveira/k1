<?php
/*
Plugin Name: WooCommerce Checkout Multi-Step
Plugin URI: https://codecanyonwp.com/5-styles-multistep-checkout-for-woocommerce/
Description:  Provides step by step UI for page checkout WooCommerce
Version: 1.3
Author: Rednumber
Author URI: https://codecanyonwp.com
*/
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
define( 'MULTISTEP_CHECKOUT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'MULTISTEP_CHECKOUT_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'MULTISTEP_CHECKOUT_TEXT_DOMAIN', "multistep_checkout" );
/*
* Include pib
*/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    include MULTISTEP_CHECKOUT_PLUGIN_PATH."backend/index.php";
    include MULTISTEP_CHECKOUT_PLUGIN_PATH."frontend/index.php";
}
/*
* Check plugin wooo
*/
class multistep_checkout_init {
    function __construct(){
       add_action('admin_notices', array($this, 'on_admin_notices' ) );
    }
    function on_admin_notices(){
        if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
            echo '<div class="error"><p>' . __('Plugin need active plugin woocommerce', MULTISTEP_CHECKOUT_TEXT_DOMAIN) . '</p></div>';
        }
    }
}
new multistep_checkout_init;