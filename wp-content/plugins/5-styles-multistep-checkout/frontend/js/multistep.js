jQuery(document).ready(function($){
    $(".checkout-display-steps-container li").click(function(e){
        $(".checkout-display-steps-container li").removeClass("active");
        $(this).addClass("active");

        $(".checkout-display-steps-container li").removeClass('enabled');
        var i = $(this).data("i");
        for( j=1;j<i;j++){
            $(".checkout-steps-"+j).addClass("enabled");
        }
        /*
        * Tab
        */
        $(".checkout-tab").addClass("hidden");
        var tab = $(this).data("tab");
        $(tab).removeClass("hidden");
        return false;
    })
    $(".multistep-checkout-prev").click(function(e){
        var current_step = parseInt( $(".checkout-display-steps-container li.active").data("i") ) - 1;
        $(".checkout-steps-"+current_step).click();
        return false;
    })
    $(".multistep-checkout-next").click(function(e){
        var current_step = parseInt( $(".checkout-display-steps-container li.active").data("i") ) + 1;
        var tab = $(".checkout-display-steps-container li.active").data("tab");
        var check_required = 0;
        if( tab == '.checkout-tab-billing'){
            var inputs =  $(".checkout-tab-billing .woocommerce-invalid-required-field input");
            if( inputs.length < 1 ){
                 var inputs =  $(".checkout-tab-billing .validate-required input")
            }
            inputs.each(function( index ) {
                    var vl = $(this).val();
                    if( vl == "" ){
                        check_required = 1;
                        $("form.checkout").submit();
                        return false;
                    }
              })
        }
        console.log(check_required);
        if(check_required != 1) {
           $(".checkout-steps-"+current_step).click();
        }

        return false;
    })
})