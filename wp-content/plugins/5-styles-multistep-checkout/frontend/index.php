<?php
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
/*
* Class frontend
*/
class multistep_checkout_frontend {
    function __construct(){
        add_action("wp_enqueue_scripts",array($this,"add_lib"));
        add_action( 'plugins_loaded', array($this,'custom_hook_form_coupon'));
        add_filter( 'woocommerce_locate_template', array($this,'woocommerce_locate_template'), 999999, 3 );
        add_action("wp_head",array($this,"cusstom_css_checkout"));
    }
    /*
    * Add css admin
    */
    function add_lib(){
        wp_enqueue_script("jquery");
        wp_enqueue_style( 'checkout-multistep', MULTISTEP_CHECKOUT_PLUGIN_URL."frontend/css/multistep.css" );
        wp_enqueue_script( 'checkout-multistep', MULTISTEP_CHECKOUT_PLUGIN_URL."frontend/js/multistep.js",array(),time()  );
    }
    /*
    * Custom hook
    */
    function custom_hook_form_coupon(){
        remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
        add_action("woocommerce_coupon_checkout_form","woocommerce_checkout_coupon_form",10);

        remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
        add_action("woocommerce_note_checkout_form","woocommerce_checkout_coupon_form",10);
    }
    /*
    * Overice template
    */
    function woocommerce_locate_template( $template, $template_name, $template_path){
        if ($template_name == 'checkout/form-checkout.php') {
            $template = MULTISTEP_CHECKOUT_PLUGIN_PATH.'woocommerce/checkout/form-checkout.php';
        }
        return $template;
    }
    /*
    * Custom css
    */
    function cusstom_css_checkout(){
        $settings = multisteps_get_settings();
        ?>
        <style type="text/css">
            .checkout-display-steps-container li {
                background: <?php echo $settings["multistep_checkout_steps_background"] ?>;
                color: <?php echo $settings["multistep_checkout_steps_color"] ?>;
            }
            .multistep_checkout_steps_color li.active{
                background: <?php echo $settings["multistep_checkout_steps_inactive_background"] ?>;
                color: <?php echo $settings["multistep_checkout_steps_inactive"] ?>;
            }
            .checkout-display-steps-container li.enabled {
                background: <?php echo $settings["multistep_checkout_steps_completed_backgound"] ?>;
                color: <?php echo $settings["multistep_checkout_steps_completed"] ?>;
            }
            .multistep-nav a{
                background: <?php echo $settings["multistep_checkout_steps_background"] ?>;
                color: <?php echo $settings["multistep_checkout_steps_color"] ?>;
                padding: 5px 15px;
                text-decoration: none;
            }
            .multistep-nav {
                    display: flex;
                    margin: 30px 0;
            }
            .multistep-nav div {
                width: 100%;
            }
            .multistep-nav-right {
                text-align: right;
            }
        </style>
        <?php
    }
}
new multistep_checkout_frontend;