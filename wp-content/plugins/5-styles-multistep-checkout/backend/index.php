<?php
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
/*
* Get settings plugin
*/
function multisteps_get_settings(){
     return array(
            "multistep_checkout_style" => get_option("multistep_checkout_style",1),
            "multistep_checkout_steps_background"=>get_option("multistep_checkout_steps_background","#B6D7EA"),
            "multistep_checkout_steps_color"=>get_option("multistep_checkout_steps_color","#666"),
            "multistep_checkout_steps_inactive_background"=>get_option("multistep_checkout_steps_inactive_background","#5fc562"),
            "multistep_checkout_steps_inactive"=>get_option("multistep_checkout_steps_inactive","#fff"),
            "multistep_checkout_steps_completed"=>get_option("multistep_checkout_steps_completed","#fff"),
            "multistep_checkout_steps_completed_backgound" => get_option("multistep_checkout_steps_completed_backgound","#3491C4"),
            "multistep_checkout_button_next" => get_option("multistep_checkout_button_next","Next"),
            "multistep_checkout_button_prev" => get_option("multistep_checkout_button_prev","Previous"),
            "multistep_checkout_button_acount"=>get_option("multistep_checkout_button_acount","No Acount"),
            "multistep_checkout_login"=>get_option("multistep_checkout_login","yes"),
            "multistep_checkout_login_text"=>get_option("multistep_checkout_login_text","Login"),
            "multistep_checkout_coupon"=>get_option("multistep_checkout_coupon","yes"),
            "multistep_checkout_coupon_text"=>get_option("multistep_checkout_coupon_text","Coupon"),
            "multistep_checkout_billing_text"=>get_option("multistep_checkout_billing_text","Billing"),
            "multistep_checkout_shipping_text"=>get_option("multistep_checkout_shipping_text","Shipping"),
            "multistep_checkout_payment_text"=>get_option("multistep_checkout_payment_text","Payment")

        );
}
/*
* Classs backend
*/
class multistep_checkout_settings_backend {
    function __construct() {
        add_filter( 'woocommerce_settings_tabs_array', array($this,"tab_settings"),100 );
        add_action( 'woocommerce_settings_tabs_woo_multistep_checkout',array($this,"settings_tab") );
        add_action( 'admin_enqueue_scripts', array($this,"add_lib") );
        add_action( 'admin_init', array($this,'update_settings' ));
    }
    /*
    * Add lib admin
    */
    function add_lib(){
        wp_enqueue_style( 'multistep_checkout', MULTISTEP_CHECKOUT_PLUGIN_URL . 'backend/css/multistep_checkout.css', false, '1.0.0' );
        wp_enqueue_script( 'checkout-multistep', MULTISTEP_CHECKOUT_PLUGIN_URL."backend/js/multistep_checkout.js",array(),time()  );
        wp_enqueue_script("wp-color-picker");
        wp_enqueue_style( 'wp-color-picker' );
    }
    /*
    * Update settings
    */
    function update_settings(){
        if( isset($_POST["multistep_checkout_style"])){
            $settings = multisteps_get_settings();
            foreach ( $settings as $key=>$value ) {
                @update_option($key,$_POST[$key]);
            }
        }

    }
    /*
    * Add tab settings
    */
    public function  tab_settings( $tabs ) {
        $tabs['woo_multistep_checkout'] = __( 'Multistep Checkout', MULTISTEP_CHECKOUT_TEXT_DOMAIN);
			return $tabs;
    }
    /*
    * Settings Tab
    */
    function settings_tab( ) {
        $settings = multisteps_get_settings();
        ?>
        <ul class="multistep-header">
            <li><a data-tab="multistep-checkout-general" class="active" href="#"><?php _e("General",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
            <li><a data-tab="multistep-checkout-login" href="#"><?php _e("Login",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
            <li><a data-tab="multistep-checkout-coupon" href="#"><?php _e("Coupon",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
            <li><a data-tab="multistep-checkout-billing" href="#"><?php _e("Billing",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
            <li><a data-tab="multistep-checkout-shipping" href="#"><?php _e("Shipping",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
            <li><a data-tab="multistep-checkout-payment" href="#"><?php _e("Payment",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></a></li>
        </ul>
        <div id="poststuff">
            <div class="multistep-checkout multistep-checkout-general">
                <h3><?php _e("General",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                		<tr>
                			<th scope="row">
                				<label for="multistep_checkout_style">
                					<?php _e("Choose Style",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<select name="multistep_checkout_style">
                                    <option value="1"><?php _e("Style 1",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></option>
                                    <option <?php selected(2,$settings["multistep_checkout_style"]) ?> value="2"><?php _e("Style 2",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></option>
                                    <option <?php selected(3,$settings["multistep_checkout_style"]) ?> value="3"><?php _e("Style 3",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></option>
                                    <option <?php selected(4,$settings["multistep_checkout_style"]) ?> value="4"><?php _e("Style 4",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></option>
                                    <option <?php selected(5,$settings["multistep_checkout_style"]) ?> value="5"><?php _e("Style 5",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></option>
                                </select>
                                <img src="" alt="" />
                			</td>
                		</tr>
                	</tbody>
                </table>
                <h3><?php _e("Steps Customization",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_background">
                					<?php _e("Steps background",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_background" class="color" value="<?php echo $settings["multistep_checkout_steps_background"] ?>" />
                			</td>
                		</tr>
                		<tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_color">
                					<?php _e("Steps color",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_color" class="color" value="<?php echo $settings["multistep_checkout_steps_color"] ?>" />
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_inactive_background">
                					<?php _e("Background for inactive steps",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_inactive_background" class="color" value="<?php echo $settings["multistep_checkout_steps_inactive_background"] ?>" />
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_inactive">
                					<?php _e("Color for inactive steps",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_inactive" class="color" value="<?php echo $settings["multistep_checkout_steps_inactive"] ?>" />
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_completed_backgound">
                					<?php _e("Completed steps background",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_completed_backgound" class="color" value="<?php echo $settings["multistep_checkout_steps_completed_backgound"] ?>" />
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_steps_completed">
                					<?php _e("Completed steps color",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input type="text" name="multistep_checkout_steps_completed" class="color" value="<?php echo $settings["multistep_checkout_steps_completed"] ?>" />
                			</td>
                		</tr>

                	</tbody>
                 </table>
                 <h3><?php _e("Button Text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                		<tr>
                			<th scope="row">
                				<label for="multistep_checkout_button_next">
                					<?php _e("Next Button",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_button_next" type="text" value="<?php echo $settings["multistep_checkout_button_next"] ?>" class="regular-text">
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_button_prev">
                					<?php _e("Previous Button",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_button_prev" type="text" value="<?php echo $settings["multistep_checkout_button_prev"] ?>" class="regular-text">
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_button_acount">
                					<?php _e("No Acount Button",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_button_acount" type="text" value="<?php echo $settings["multistep_checkout_button_acount"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
            <div class="multistep-checkout multistep-checkout-login hidden">
                <h3><?php _e("Login",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                		<tr>
                			<th scope="row">
                				<label for="multistep_checkout_login">
                					<?php _e("Enable",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_login" type="checkbox" value="yes" <?php checked( "yes",$settings["multistep_checkout_login"] ) ?>  /> <label>Enable</label>
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_login_text">
                					<?php _e("Custom text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_login_text" type="text" value="<?php echo $settings["multistep_checkout_login_text"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
            <div class="multistep-checkout multistep-checkout-coupon hidden">
                <h3><?php _e("Coupon",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                		<tr>
                			<th scope="row">
                				<label for="multistep_checkout_coupon">
                					<?php _e("Enable",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_coupon" type="checkbox" value="yes" <?php checked( "yes",$settings["multistep_checkout_coupon"] ) ?> /> <label>Enable</label>
                			</td>
                		</tr>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_coupon_text">
                					<?php _e("Custom text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_coupon_text" type="text" value="<?php echo $settings["multistep_checkout_coupon_text"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
            <div class="multistep-checkout multistep-checkout-billing hidden">
                <h3><?php _e("Billing",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_billing_text">
                					<?php _e("Custom text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_billing_text" type="text" value="<?php echo $settings["multistep_checkout_billing_text"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
            <div class="multistep-checkout multistep-checkout-shipping hidden">
                <h3><?php _e("Shipping",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_shipping_text">
                					<?php _e("Custom text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_shipping_text" type="text" value="<?php echo $settings["multistep_checkout_shipping_text"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
            <div class="multistep-checkout multistep-checkout-payment hidden">
                <h3><?php _e("Payment",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?></h3>
                <table class="form-table">
                	<tbody>
                        <tr>
                			<th scope="row">
                				<label for="multistep_checkout_payment_text">
                					<?php _e("Custom text",MULTISTEP_CHECKOUT_TEXT_DOMAIN) ?>
                				</label>
                			</th>
                			<td>
                				<input name="multistep_checkout_payment_text" type="text" value="<?php echo $settings["multistep_checkout_payment_text"] ?>" class="regular-text">
                			</td>
                		</tr>
                	</tbody>
                 </table>
            </div>
        </div>
        <?php
    }
}
new  multistep_checkout_settings_backend;