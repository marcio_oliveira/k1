jQuery(document).ready(function($){
    $(".multistep-header li a").click(function(e){
        $(".multistep-header li a").removeClass("active");
        $(this).addClass("active");
        /*
        * Tab
        */
        $(".multistep-checkout").addClass("hidden");
        var tab = $(this).data("tab");
        $("."+tab).removeClass("hidden");
        return false;
    })
    $('.color').wpColorPicker();
})