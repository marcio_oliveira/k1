<?php
/*
Plugin Name: Divi Commerce
Plugin URI: https://www.boltthemes.com/
Description: Make your product pages stand out to your customers
Author: Bolt Themes
Version: 2.0.4
Author URI: https://www.boltthemes.com/
*/
require plugin_dir_path( __FILE__ ) .  '/update/update-checker.php';
$MyUpdateChecker = PucFactory::buildUpdateChecker(
    'https://boltthemes.com/updates/?action=get_metadata&slug=divicommerce',
    __FILE__, 
    'divicommerce'
);

/*Including files*/
/*Enable Pagebuilder*/	
require plugin_dir_path( __FILE__ ) . '/includes/enable-pgb.php';
require plugin_dir_path( __FILE__ ) . '/meta-box/meta-box.php'; // Path to the plugin's main file
//require plugin_dir_path( __FILE__ ) . '/meta-box/ext/mb-term-meta/mb-term-meta.php';

/*Plugin options*/
include("includes/single-product-meta.php");
include("includes/customizer.php"); 
//include("includes/shortcodes.php");
/*Working on feature*/
/*include("includes/category-layout.php");*/ 

/*Divi Commerce modules*/
function DC_Custom_Modules(){
 if(class_exists("ET_Builder_Module")){
 include("modules/addtocart_module.php");
 include("modules/info_tab_module.php");
 include("modules/reviews_module.php");
 include("modules/price_module.php");
 include("modules/title_module.php");
 /*include("modules/myaccount.php");
 include("modules/cart.php");
 include("modules/checkout.php");*/
 }
}
function Prep_DC_Custom_Modules(){
 global $pagenow;

$is_admin = is_admin();
 $action_hook = $is_admin ? 'wp_loaded' : 'wp';
 $required_admin_pages = array( 'edit.php', 'post.php', 'post-new.php', 'admin.php', 'customize.php', 'edit-tags.php', 'admin-ajax.php', 'export.php' ); // list of admin pages where we need to load builder files
 $specific_filter_pages = array( 'edit.php', 'admin.php', 'edit-tags.php' );
 $is_edit_library_page = 'edit.php' === $pagenow && isset( $_GET['post_type'] ) && 'et_pb_layout' === $_GET['post_type'];
 $is_role_editor_page = 'admin.php' === $pagenow && isset( $_GET['page'] ) && 'et_divi_role_editor' === $_GET['page'];
 $is_import_page = 'admin.php' === $pagenow && isset( $_GET['import'] ) && 'wordpress' === $_GET['import']; 
 $is_edit_layout_category_page = 'edit-tags.php' === $pagenow && isset( $_GET['taxonomy'] ) && 'layout_category' === $_GET['taxonomy'];

if ( ! $is_admin || ( $is_admin && in_array( $pagenow, $required_admin_pages ) && ( ! in_array( $pagenow, $specific_filter_pages ) || $is_edit_library_page || $is_role_editor_page || $is_edit_layout_category_page || $is_import_page ) ) ) {
 add_action($action_hook, 'DC_Custom_Modules', 9789);
 }
}
Prep_DC_Custom_Modules(); 
/*Clear Module Cache*/
define( 'DC_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
function dc_clear_local_storage () {
	wp_enqueue_script( 'dc_clear_local_storage', DC_PLUGIN_URL . 'clear_local_storage.js' );
    wp_enqueue_script( 'dc_fullscreen_class', DC_PLUGIN_URL . 'includes/js/dc_class.js' );
}
add_action( 'admin_enqueue_scripts', 'dc_clear_local_storage', 9999 );
/*template overide*/
function dc_custom_css() {
	wp_register_style( 'divi_commerce_css', plugins_url('style.css', __FILE__ ),'','1.1', '' );
	 wp_enqueue_style( 'divi_commerce_css' );
}
add_action( 'wp_enqueue_scripts', 'dc_custom_css', '30' );
function ds_dc_admin_css() {

    // Back End CSS
    wp_enqueue_style('back-end-css', plugin_dir_url( __FILE__ ) . 'includes/css/backend.css');
}
add_action('admin_enqueue_scripts', 'ds_dc_admin_css'); // Admin Assets  
//Replace the default Single Product template with ours
if( true != get_theme_mod( 'dc_no_template' ) ) {
function dc_single_product_file_replace() {
   $plugin_dir = plugin_dir_path( __FILE__ ) . '/single-product.php';
   $theme_dir = get_stylesheet_directory() . '/single-product.php';

    if (!copy($plugin_dir, $theme_dir)) { 
      echo "failed to copy $plugin_dir to $theme_dir...\n";
    }

}
add_action('wp_head', 'dc_single_product_file_replace');
}
/*Feature being worked on*/
/*function dc_woo_category_file_replace() {

   $plugin_dir = plugin_dir_path( __FILE__ ) . '/taxonomy-product_cat.php';
   $theme_dir = get_stylesheet_directory() . '/taxonomy-product_cat.php';

    if (!copy($plugin_dir, $theme_dir)) {
      echo "failed to copy $plugin_dir to $theme_dir...\n";
    }
}

add_action('wp_head', 'dc_woo_category_file_replace');*/
/*Remove checkout feilds*/
add_filter( 'woocommerce_checkout_fields' , 'ds_dc_override_fields' );
function ds_dc_override_fields( $fields ) {
if( false != get_theme_mod( 'dc_remove_first_name' ) ) {
unset($fields['billing']['billing_first_name']);
}
if( false != get_theme_mod( 'dc_remove_last_name' ) ) {
unset($fields['billing']['billing_last_name']);
}
if( false != get_theme_mod( 'dc_remove_company' ) ) {
unset($fields['billing']['billing_company']);
}
if( false != get_theme_mod( 'dc_remove_address_1' ) ) {
unset($fields['billing']['billing_address_1']);
}
if( false != get_theme_mod( 'dc_remove_address_2' ) ) {
unset($fields['billing']['billing_address_2']);
}
if( false != get_theme_mod( 'dc_remove_city' ) ) {
unset($fields['billing']['billing_city']);
}
if( false != get_theme_mod( 'dc_remove_post_code' ) ) {
unset($fields['billing']['billing_postcode']);
}
if( false != get_theme_mod( 'dc_remove_country' ) ) {
unset($fields['billing']['billing_country']);
}
if( false != get_theme_mod( 'dc_remove_state' ) ) {
unset($fields['billing']['billing_state']);
}
if( false != get_theme_mod( 'dc_remove_phone' ) ) {
unset($fields['billing']['billing_phone']);
}
if( false != get_theme_mod( 'dc_remove_order_comments' ) ) {
unset($fields['order']['order_comments']);
}
if( false != get_theme_mod( 'dc_remove_email' ) ) {
unset($fields['billing']['billing_email']);
}
return $fields;
}
//Add to cart on shop pages and shop module
if( false != get_theme_mod( 'dc_add_act1_loop' ) ) {
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 20 );
}
if( false != get_theme_mod( 'dc_add_vdb_loop' ) ) {
add_action( 'woocommerce_after_shop_loop_item', 'bbloomer_view_product_button', 30);
function bbloomer_view_product_button() {
global $product;
$link = $product->get_permalink();
echo do_shortcode('<a href="'.$link.'" class="button addtocartbutton">View Product</a>');
}
}
function ds_dc_section_injector() { ?>
    <!-- Above the header -->
    <?php if( false != get_theme_mod( 'dc_add_header_atc' ) ) { ?>
    <li id="header-atc">
        <?php if ( is_singular( 'product' ) ) {
do_action( 'woocommerce_simple_add_to_cart', 1 ); 
       }  ?>
    </li> <?php } ?>
    <!--mini cart-->
    <?php if( false != get_theme_mod( 'dc_add_header_cart' ) ) { ?>
    <div id="cartcontents">
        <div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div>
    </div>
    <?php } ?> 
    <?php if( false != get_theme_mod( 'dc_add_bottom_product_info' ) ) { ?>
    <div id="product-info-bottom">
        <?php if ( is_singular( 'product' ) ) { ?>
    <div>
    <?php 
        woocommerce_show_product_sale_flash();
		woocommerce_show_product_images (); 
    ?>
    </div>
    <div class="summary entry-summary">
    <?php
		woocommerce_template_single_title ();
		woocommerce_template_single_price ();
		woocommerce_template_single_excerpt ();
		woocommerce_template_single_add_to_cart ();
		woocommerce_template_single_meta ();
		?>
        </div>
     <?php    }  ?>
    </div>
    <?php } ?>
<script>
        jQuery(function($){ 
            <?php if( false != get_theme_mod( 'dc_add_bottom_product_info' ) ) { ?>
            $('.wc-tabs-wrapper').after($('#product-info-bottom'));
            $("#product-info-bottom").show();
            <?php } ?>
            <?php if( false != get_theme_mod( 'dc_add_header_atc' ) ) { ?>
            // Above header - Added inside #main-header wrap
            $("#top-menu").append($("#header-atc"));
            $("#header-atc").show();
        <?php } 
            //Insert mini hover cart
            if( false != get_theme_mod( 'dc_add_header_cart' ) ) { ?>
            $(".et-cart-info").append($("#cartcontents"));
            $("#cartcontents").show();
            <?php } ?>
            });
    </script>
<?php 
} 
add_action('wp_footer', 'ds_dc_section_injector');