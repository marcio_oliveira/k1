<?php
class dc_cart_module extends ET_Builder_Module {
                function init() {
                    $this->name = __( 'Cart', 'et_builder' );
                    $this->slug = 'et_pb_cart';

                    $this->whitelisted_fields = array(
                        'title',
                        'module_id',
                        'module_class',
                    );
                    $this->fields_defaults = array();
					$this->options_toggles = array(
                        'general' => array(
                            'toggles' => array(
                                'remove_fields' => esc_html__('Remove Fields', 'et_builder'),
                            ),
                        ),
                        'advanced' => array(
                            'toggles' => array(
                                'notices' => esc_html__('Notices', 'et_builder'),
                                'forms' => esc_html__('Form Styler', 'et_builder'),
                                'tables' => esc_html__('Tables', 'et_builder'),
                                'payment_box' => esc_html__('Payment Box', 'et_builder'),
                            ),
                        ),
                        'custom_css' => array(
                            'toggles' => array(

                            ),
                        ),
                    );

                    $this->main_css_element = '%%order_class%%';
                    $this->advanced_options = array(

                    );
                    $this->custom_css_options = array(

                    );
                }
                function get_fields() {
                    $fields = array(
                        'module_id' => array(
				            'label'           => esc_html__( 'CSS ID', 'et_builder' ),
				            'type'            => 'text',
				            'option_category' => 'configuration',
				            'tab_slug'        => 'custom_css',
				            'option_class'    => 'et_pb_custom_css_regular',
							'toggle_slug'        => 'classes',
			            ),
			            'module_class' => array(
				            'label'           => esc_html__( 'CSS Class', 'et_builder' ),
				            'type'            => 'text',
				            'option_category' => 'configuration',
				            'tab_slug'        => 'custom_css',
				            'option_class'    => 'et_pb_custom_css_regular',
							'toggle_slug'        => 'classes',
			            ),
                    );

                    return $fields;
                }

                function shortcode_callback( $atts, $content = null, $function_name ) {
                    $module_id          = $this->shortcode_atts['module_id'];
                    $module_class       = $this->shortcode_atts['module_class'];



                    $module_class = ET_Builder_Element::add_module_order_class( $module_class, $function_name );

                    ///////////////////////////////////////////////////////////////////////////
                    
                    ob_start();
                        echo do_shortcode( '[woocommerce_cart]' );
                    $content = ob_get_clean();

                    //////////////////////////////////////////////////////////////////////////

                     $output = sprintf(
                    '<div%5$s class="et_pb_module et_pb_cart %1$s%3$s">
                        %2$s
                        %4$s',
                        'clearfix ',
                        $content,
                        '</div>',
                        ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
						( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' )
                    );
            
                    return $output;
                    }
                };
        
            new dc_cart_module();
?>