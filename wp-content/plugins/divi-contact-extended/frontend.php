<?php
add_filter('wp_footer', 'trumani_captcha_form', 15, 2);
function trumani_captcha_form($content)
{
	?>
	<script type="text/javascript">
	var tcap_sitekey = "<?php echo get_option('tcap_sitekey'); ?>";

	var tcap_theme = "<?php echo get_option('tcap_theme'); ?>";
	if(tcap_theme == '') { tcap_theme = 'light'; }

	var tcap_invisible = "<?php echo get_option('tcap_invisible'); ?>";
	if(tcap_invisible == '') { tcap_invisible = 'no'; }

	var tcap_contactform = "<?php echo get_option('tcap_contactform'); ?>";
	if(tcap_contactform == '') { tcap_contactform = 'no'; }

	var tcap_optin = "<?php echo get_option('tcap_optin'); ?>";
	if(tcap_optin == '') { tcap_optin = 'no'; }

	var tcap_addphonefield_maskenable = "<?php echo get_option('tcap_addphonefield_maskenable'); ?>";
	if(tcap_addphonefield_maskenable == '') { tcap_addphonefield_maskenable = 'no'; }
	var tcap_addphonefield_maskpattern = "<?php echo get_option('tcap_addphonefield_maskpattern'); ?>";
	
	if(tcap_invisible == 'yes')
	{
		if(tcap_contactform == 'yes')
		{
			jQuery("form.et_pb_contact_form").each(function(index)
			{
				jQuery(this).find('.et_contact_bottom_container').prepend('<div class="col-md-12 trumani_con"><div class="g-recaptcha" data-theme="'+tcap_theme+'" data-sitekey="'+tcap_sitekey+'" data-size="invisible" data-callback="etContactSubmit"></div></div>');
			});
		}
		if(tcap_optin == 'yes')
		{
			jQuery('.et_pb_newsletter_form .et_pb_newsletter_button').parent().prepend('<div class="col-md-12 trumani_con"><div class="g-recaptcha" data-theme="'+tcap_theme+'" data-sitekey="'+tcap_sitekey+'" data-size="invisible" data-callback="etEmailOptinSubmit"></div></div>');
		}
	}
	else
	{
		if(tcap_contactform == 'yes')
		{
			jQuery("form.et_pb_contact_form").each(function(index)
			{
				jQuery(this).find('.et_contact_bottom_container').prepend('<div class="col-md-12 trumani_con"><div class="g-recaptcha trumani" data-theme="'+tcap_theme+'" data-sitekey="'+tcap_sitekey+'"></div></div>');
			});
		}
		if(tcap_optin == 'yes')
		{
			jQuery('.et_pb_newsletter_form .et_pb_newsletter_button').parent().prepend('<div class="col-md-12 trumani_con"><div class="g-recaptcha trumani" data-theme="'+tcap_theme+'" data-sitekey="'+tcap_sitekey+'"></div></div>');
		}
	}
	</script>
	<link rel="stylesheet" href="<?php echo plugins_url('css/frontend.css',__FILE__); ?>" />
	<script type="text/javascript" src="<?php echo plugins_url('js/frontend.js',__FILE__); ?>" ></script>
	<script type="text/javascript">
		if(tcap_addphonefield_maskenable == 'yes' && tcap_addphonefield_maskpattern != '')
		{
			jQuery(document).ready(function()
			{
				jQuery('.tonabophonemask').mask(tcap_addphonefield_maskpattern, {placeholder: tcap_addphonefield_maskpattern});
			});
		}
	</script>
	<style>
	.et_contact_bottom_container .et_pb_contact_right { display:none; }
	.et_pb_contact_form .trumani_con { margin-bottom:10px; }
	.et_pb_newsletter_form .trumani_con { margin-bottom:10px; }
	<?php
	if(wp_is_mobile())
	{
	?>
	.g-recaptcha.trumani { transform:scale(0.69); -webkit-transform:scale(0.69); transform-origin:100% 0; -webkit-transform-origin:100% 0; }
	<?php
	}
	?>
	</style>
	<?php
}

add_filter('wp_footer', 'trumani_captcha_js', 15, 2);
function trumani_captcha_js()
{
	?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript">
	jQuery("form.et_pb_contact_form").each(function(index)
	{
		if(jQuery(this).find(".et_contact_bottom_container .et_pb_contact_right").length > 0)
		{
			var mathcaptchastr = jQuery(this).find('.et_contact_bottom_container .et_pb_contact_captcha_question').html();
			var mathcaptchaarr = mathcaptchastr.split(" ");
			var mathcaptchans = parseInt(mathcaptchaarr[0]) + parseInt(mathcaptchaarr[2]);
			jQuery(this).find('.et_contact_bottom_container .et_pb_contact_captcha').val(parseInt(mathcaptchans));
			jQuery(this).find(".et_contact_bottom_container .et_pb_contact_right").hide();
		}
		jQuery(this).find(".et_pb_contact_submit").click(function(e)
		{
			if(tcap_invisible == 'no' && tcap_contactform == 'yes')
			{
				var tcap_flag1;
				jQuery.ajax(
				{
					type: "POST",
					url: "<?php echo get_site_url(); ?>/wp-content/plugins/trumani/google_captcha.php",
					data: jQuery(this).parents('form.et_pb_contact_form').serialize(),
					async: false,
					success: function(data)
					{
						if(data.nocaptcha === "true")
						{
							tcap_flag1 = 1;
						}
						else if(data.spam === "true")
						{
							tcap_flag1 = 1;
						}
						else
						{
							tcap_flag1 = 0;
						}
					}
				});
				if(tcap_flag1 != 0)
				{
					e.preventDefault();
					if(tcap_flag1 == 1)
					{
						alert("Please check reCAPTCHA");
					}
					else
					{
						jQuery(this).find(".et_pb_contact_form").submit();
					}
				}
				else
				{
					jQuery(this).find(".et_pb_contact_form").submit();
				}
			}
		});
	});
	function etContactSubmit(token)
	{
		jQuery(".et_pb_contact_form").submit();
	}
	jQuery(".et_pb_newsletter_button").click(function(e)
	{
		if(tcap_invisible == 'no' && tcap_optin == 'yes')
		{
			var tcap_flag2;
			jQuery.ajax(
			{
				type: "POST",
				url: "<?php echo get_site_url(); ?>/wp-content/plugins/trumani/google_captcha.php",
				data: { 'g-recaptcha-response' : jQuery('.g-recaptcha-response').val()},
				async: false,
				success: function(data)
				{
					if(data.nocaptcha === "true")
					{
						tcap_flag2 = 1;
					}
					else if(data.spam === "true")
					{
						tcap_flag2 = 1;
					}
					else
					{
						tcap_flag2 = 0;
					}
				}
			});
			if(tcap_flag2 != 0)
			{
				e.preventDefault();
				if(tcap_flag2 == 1)
				{
					alert("Please check reCAPTCHA");
				}
				else
				{
					jQuery(".et_pb_newsletter_form").submit();
				}
			}
			else
			{
				jQuery(".et_pb_newsletter_form").submit();
			}
		}
	});
	function etEmailOptinSubmit(token)
	{
		jQuery(".et_pb_newsletter_form").submit();
	}
	</script>
	<?php
}
?>