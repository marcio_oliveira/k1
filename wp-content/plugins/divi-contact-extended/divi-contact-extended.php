<?php
/*
Plugin Name: Divi Contact Extended
Plugin URI: https://trumani.com/divi-contact-module-google-recaptcha-plugin/
Description: Adding Google Recaptcha functionality to Divi contact forms and you able add more fileds
Version: 1.0
Author: Trumani
Author URI: https://trumani.com
License: GPLv2+
*/

define('TRUMANI_PLUGIN_NAME', 'Divi Contact Extended');

add_action('admin_menu', 'trumani_create_menu');
function trumani_create_menu()
{
	add_submenu_page('options-general.php', TRUMANI_PLUGIN_NAME.' Settings', TRUMANI_PLUGIN_NAME, 'administrator', __FILE__, 'trumani_setting_page');
	add_action('admin_init', 'trumani_plugin_settings');
}

require_once('backend.php');

require_once('frontend.php');

//require_once(get_theme_root().'/Divi/includes/builder/main-modules.php');
require_once('divi_contact_form_extended.php');

?>