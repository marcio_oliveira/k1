<?php
function trumani_setting_page()
{
	?>
	<div class="wrap">

		<h1><?php echo TRUMANI_PLUGIN_NAME; ?> Settings</h1>
		<hr />

		<form method="post" action="options.php" onSubmit="makeEmailRouting();">

			<?php settings_fields('trumani_form_field_list'); ?>
			<?php do_settings_sections('trumani_form_field_list'); ?>

			<h2>General Settings</h2>
			<p>* Enter <a href="https://www.google.com/recaptcha" target="_blank">Google reCAPTCHA v2, Invisible reCAPTCHA</a> key settings below.</p>
			<table class="form-table">
				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Site Key (Public)</th>
					<td><input type="text" name="tcap_sitekey" value="<?php echo esc_attr(get_option('tcap_sitekey')); ?>" /></td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Secret Key (Private)</th>
					<td><input type="text" name="tcap_secretkey" value="<?php echo esc_attr(get_option('tcap_secretkey')); ?>" /></td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Theme</th>
					<td>
						<input type="radio" name="tcap_theme" value="light" <?php if(esc_attr(get_option('tcap_theme')) == '' || esc_attr(get_option('tcap_theme')) == 'light') { echo 'checked="checked"'; } ?> /> Light
						&nbsp;
						<input type="radio" name="tcap_theme" value="dark" <?php if(esc_attr(get_option('tcap_theme')) == 'dark') { echo 'checked="checked"'; } ?> /> Dark
					</td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Invisible reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_invisible" value="no" <?php if(esc_attr(get_option('tcap_invisible')) == '' || esc_attr(get_option('tcap_invisible')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_invisible" value="yes" <?php if(esc_attr(get_option('tcap_invisible')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>
			</table>
			
			<hr />
			<h2>Divi Contact Form</h2>
			<p>* If <strong><?php echo TRUMANI_PLUGIN_NAME; ?></strong> plugin enabled then default Divi's theme captcha automatically ignored.</p>
			<table class="form-table">

				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Enable reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_contactform" value="no" <?php if(esc_attr(get_option('tcap_contactform')) == '' || esc_attr(get_option('tcap_contactform')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_contactform" value="yes" <?php if(esc_attr(get_option('tcap_contactform')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Add Phone Field</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_addphonefield" value="yes" <?php if(esc_attr(get_option('tcap_addphonefield')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<input type="checkbox" name="tcap_addphonefield_required" value="yes" <?php if(esc_attr(get_option('tcap_addphonefield_required')) == 'yes') { echo 'checked="checked"'; } ?> /> Required
						</div>
						<div class="row">
							<label><strong>Field Label</strong></label>
							<br />
							<input type="text" name="tcap_addphonefield_label" value="<?php echo esc_attr(get_option('tcap_addphonefield_label')); ?>" />
						</div>
						<div class="row">
							<input type="checkbox" name="tcap_addphonefield_maskenable" value="yes" <?php if(esc_attr(get_option('tcap_addphonefield_maskenable')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable Input Mask
							<p>*If this option is enabled then you must enter any mask pattern and if you left empty then not apply on form</p>
						</div>
						<div class="row">
							<label><strong>Input Mask Pattern</strong></label>
							<br />
							<input type="text" name="tcap_addphonefield_maskpattern" value="<?php echo esc_attr(get_option('tcap_addphonefield_maskpattern')); ?>" />
							<p>Add phone input pattern (i.e. (000) 000-0000, 000-000-0000  etc.)</p>
						</div>
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Add File Upload Field</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_addfileuploadfield" value="yes" <?php if(esc_attr(get_option('tcap_addfileuploadfield')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<input type="checkbox" name="tcap_addfileuploadfield_required" value="yes" <?php if(esc_attr(get_option('tcap_addfileuploadfield_required')) == 'yes') { echo 'checked="checked"'; } ?> /> Required
						</div>
						<div class="row">
							<label><strong>Field Label</strong></label>
							<br />
							<input type="text" name="tcap_addfileuploadfield_label" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_label')); ?>" />
						</div>
						<div class="row">
							<label><strong>Allowed file extensions</strong></label>
							<br />
							<input type="text" name="tcap_addfileuploadfield_allowext" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_allowext')); ?>" />
							<p>Separated with commas (i.e. .jpg, .gif, .png, .pdf, image/*, video/*) and if you left empty then means all extensions allowed</p>
						</div>
						<div class="row">
							<label><strong>Multiple Files</strong></label>
							<br />
							<input type="checkbox" name="tcap_addfileuploadfield_allowmultiple" value="yes" <?php if(esc_attr(get_option('tcap_addfileuploadfield_allowmultiple')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable Multiple Files Upload
						</div>
						<div class="row">
							<label><strong>Maximum File Size</strong></label>
							<br />
							<input type="number" name="tcap_addfileuploadfield_allowmaxsize" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_allowmaxsize')); ?>" min="1" max="99" />
							<p>Upload file size unit in megabyte (MB)</p>
						</div>
					</td>
				</tr>

			</table>
			
			<hr />
			<h2>Divi Opt-in Form</h2>
			<table class="form-table">
				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Enable reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_optin" value="no" <?php if(esc_attr(get_option('tcap_optin')) == '' || esc_attr(get_option('tcap_optin')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_optin" value="yes" <?php if(esc_attr(get_option('tcap_optin')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>
			</table>
			<hr />
			
			<?php submit_button(); ?>

		</form>
	</div>
	<link rel="stylesheet" href="<?php echo plugins_url('css/backend.css',__FILE__); ?>" />
	<script type="text/javascript" src="<?php echo plugins_url('js/backend.js',__FILE__); ?>" ></script>
	<?php
}

function trumani_plugin_settings()
{
	register_setting('trumani_form_field_list', 'tcap_sitekey');
	register_setting('trumani_form_field_list', 'tcap_secretkey');
	register_setting('trumani_form_field_list', 'tcap_theme');
	register_setting('trumani_form_field_list', 'tcap_invisible');
	register_setting('trumani_form_field_list', 'tcap_contactform');
	
	register_setting('trumani_form_field_list', 'tcap_addphonefield');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_required');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_label');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_maskenable');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_maskpattern');

	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_required');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_label');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowext');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowmultiple');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowmaxsize');
	
	register_setting('trumani_form_field_list', 'tcap_optin');
}
?>