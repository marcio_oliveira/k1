<?php
include('../../../wp-config.php');
$data;
header('Content-Type: application/json');
error_reporting(E_ALL ^ E_NOTICE);
if(isset($_REQUEST['g-recaptcha-response']))
{
	$response = $_REQUEST['g-recaptcha-response'];
}
if(!$response)
{
	$data = array('nocaptcha' => 'true');
	echo json_encode($data);
	exit;
}
$remoteip = $_SERVER['REMOTE_ADDR'];
$secret = get_option('tcap_secretkey');
$cresponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response."&remoteip=".$remoteip);
if($cresponse.success == false)
{
	$data = array('spam' => 'true');
	echo json_encode($data);
}
else
{
	$data = array('spam' => 'false');
	echo json_encode($data);
}
?>