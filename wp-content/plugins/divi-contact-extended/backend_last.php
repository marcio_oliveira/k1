<?php
function trumani_setting_page()
{
	?>
	<div class="wrap">

		<h1><?php echo TRUMANI_PLUGIN_NAME; ?> Settings</h1>
		<hr />

		<form method="post" action="options.php" onSubmit="makeEmailRouting();">

			<?php settings_fields('trumani_form_field_list'); ?>
			<?php do_settings_sections('trumani_form_field_list'); ?>

			<h2>General Settings</h2>
			<p>* Enter <a href="https://www.google.com/recaptcha" target="_blank">Google reCAPTCHA v2, Invisible reCAPTCHA</a> key settings below.</p>
			<table class="form-table">
				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Site Key (Public)</th>
					<td><input type="text" name="tcap_sitekey" value="<?php echo esc_attr(get_option('tcap_sitekey')); ?>" /></td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Secret Key (Private)</th>
					<td><input type="text" name="tcap_secretkey" value="<?php echo esc_attr(get_option('tcap_secretkey')); ?>" /></td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Theme</th>
					<td>
						<input type="radio" name="tcap_theme" value="light" <?php if(esc_attr(get_option('tcap_theme')) == '' || esc_attr(get_option('tcap_theme')) == 'light') { echo 'checked="checked"'; } ?> /> Light
						&nbsp;
						<input type="radio" name="tcap_theme" value="dark" <?php if(esc_attr(get_option('tcap_theme')) == 'dark') { echo 'checked="checked"'; } ?> /> Dark
					</td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">Invisible reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_invisible" value="no" <?php if(esc_attr(get_option('tcap_invisible')) == '' || esc_attr(get_option('tcap_invisible')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_invisible" value="yes" <?php if(esc_attr(get_option('tcap_invisible')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>
				<tr valign="top" class="form-field">
					<th scope="row">reCAPTCHA Alignment</th>
					<td>
						<input type="radio" name="tcap_captchaalign" value="right" <?php if(esc_attr(get_option('tcap_captchaalign')) == '' || esc_attr(get_option('tcap_captchaalign')) == 'right') { echo 'checked="checked"'; } ?> /> Right
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_captchaalign" value="left" <?php if(esc_attr(get_option('tcap_captchaalign')) == 'left') { echo 'checked="checked"'; } ?> /> Left
					</td>
				</tr>
			</table>
			
			<hr />
			<h2>Divi Contact Form</h2>
			<p>* If <strong><?php echo TRUMANI_PLUGIN_NAME; ?></strong> plugin enabled then default Divi's theme captcha automatically ignored.</p>
			<table class="form-table">

				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Enable reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_contactform" value="no" <?php if(esc_attr(get_option('tcap_contactform')) == '' || esc_attr(get_option('tcap_contactform')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_contactform" value="yes" <?php if(esc_attr(get_option('tcap_contactform')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Submit Button Alignment</th>
					<td>
						<input type="radio" name="tcap_contactformbtnalign" value="right" <?php if(esc_attr(get_option('tcap_contactformbtnalign')) == '' || esc_attr(get_option('tcap_contactformbtnalign')) == 'right') { echo 'checked="checked"'; } ?> /> Right
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_contactformbtnalign" value="left" <?php if(esc_attr(get_option('tcap_contactformbtnalign')) == 'left') { echo 'checked="checked"'; } ?> /> Left
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Send To</th>
					<td>
						<div class="row">
							<input type="radio" name="tcap_sendto" id="tcap_sendto1" value="email" <?php if(esc_attr(get_option('tcap_sendto')) == '' || esc_attr(get_option('tcap_sendto')) == 'email') { echo 'checked="checked"'; } ?> /> Enter Email
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" name="tcap_sendto" id="tcap_sendto2" value="routing" <?php if(esc_attr(get_option('tcap_sendto')) == 'routing') { echo 'checked="checked"'; } ?> /> Configure Routing
						</div>
						<div class="row" id="tcap_sendto1_con" style="<?php if(esc_attr(get_option('tcap_sendto')) == '' || esc_attr(get_option('tcap_sendto')) == 'email') { echo 'display:block;'; } ?>">
							<input type="text" name="tcap_sendto_email" value="<?php echo esc_attr(get_option('tcap_sendto_email')); ?>" />
						</div>
						<div class="row" id="tcap_sendto2_con" style="<?php if(esc_attr(get_option('tcap_sendto')) == 'routing') { echo 'display:block;'; } ?>">

							<div class="routingrowclone">
								<div class="routingrow">
									<div class="con1">
										<input type="text" name="tcap_sendto_routing1" class="tcap_sendto_routing1" />
									</div>
									<div class="con2">if</div>
									<div class="con3">
										<select name="tcap_sendto_routing2" class="tcap_sendto_routing2">
											<option value="Select">Select</option>
										</select>
									</div>
									<div class="con4">
										<select name="tcap_sendto_routing3" class="tcap_sendto_routing3">
											<option value='is'>is</option>
										</select>
									</div>
									<div class="con5">
										<input type="text" name="tcap_sendto_routing4" class="tcap_sendto_routing4" />
									</div>
									<div class="con6">
										<a href="javascript:void(0);" class="addrule">
											<img src="<?php echo plugins_url('images/add.png',__FILE__); ?>" alt="Add New" />
										</a>
										<a href="javascript:void(0);" class="deleterule">
											<img src="<?php echo plugins_url('images/del.png',__FILE__); ?>" alt="Delete" />
										</a>
									</div>
								</div>
							</div>

							<input type="hidden" name="tcap_sendto_routing" id="tcap_sendto_routing" value="<?php echo esc_attr(get_option('tcap_sendto_routing')); ?>" />

							<div class="routingrow_container">
								<?php
								$routingstr = get_option('tcap_sendto_routing');
								if($routingstr != '')
								{
									$routingoarr = explode('|||', $routingstr);
									$routingcounter = 0;
									foreach($routingoarr as $routingov)
									{
										$routingiarr = explode('~~~', $routingov);
										?>
										<div class="routingrow">
											<div class="con1">
												<input type="text" name="tcap_sendto_routing1" class="tcap_sendto_routing1" value="<?php echo $routingiarr[0]; ?>" />
											</div>
											<div class="con2">if</div>
											<div class="con3">
												<select name="tcap_sendto_routing2" class="tcap_sendto_routing2">
													<option value="Select">Select</option>
												</select>
											</div>
											<div class="con4">
												<select name="tcap_sendto_routing3" class="tcap_sendto_routing3">
													<option value='is'>is</option>
												</select>
											</div>
											<div class="con5">
												<input type="text" name="tcap_sendto_routing4" class="tcap_sendto_routing4" value="<?php echo $routingiarr[3]; ?>" />
											</div>
											<div class="con6">
												<a href="javascript:void(0);" class="addrule">
													<img src="<?php echo plugins_url('images/add.png',__FILE__); ?>" alt="Add New" />
												</a>
												<?php
												if($routingcounter > 0)
												{
													?>
													<a href="javascript:void(0);" class="deleterule">
														<img src="<?php echo plugins_url('images/del.png',__FILE__); ?>" alt="Delete" />
													</a>
													<?php
												}
												?>
											</div>
										</div>
										<?php
										$routingcounter++;
									}
								}
								else
								{
									?>
									<div class="routingrow">
										<div class="con1">
											<input type="text" name="tcap_sendto_routing1" class="tcap_sendto_routing1" />
										</div>
										<div class="con2">if</div>
										<div class="con3">
											<select name="tcap_sendto_routing2" class="tcap_sendto_routing2">
												<option value="Select">Select</option>
											</select>
										</div>
										<div class="con4">
											<select name="tcap_sendto_routing3" class="tcap_sendto_routing3">
												<option value='is'>is</option>
											</select>
										</div>
										<div class="con5">
											<input type="text" name="tcap_sendto_routing4" class="tcap_sendto_routing4" />
										</div>
										<div class="con6">
											<a href="javascript:void(0);" class="addrule">
												<img src="<?php echo plugins_url('images/add.png',__FILE__); ?>" alt="Add New" />
											</a>
										</div>
									</div>
									<?php
								}
								?>
							</div>
						</div>
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Add File Upload Field</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_addfileuploadfield" value="yes" <?php if(esc_attr(get_option('tcap_addfileuploadfield')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<label><strong>Field Label</strong></label>
							<br />
							<input type="text" name="tcap_addfileuploadfield_label" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_label')); ?>" />
						</div>
						<div class="row">
							<label><strong>Allowed file extensions</strong></label>
							<br />
							<input type="text" name="tcap_addfileuploadfield_allowext" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_allowext')); ?>" />
							<p>Separated with commas (i.e. jpg, gif, png, pdf)</p>
						</div>
						<div class="row">
							<label><strong>Multiple Files</strong></label>
							<br />
							<input type="checkbox" name="tcap_addfileuploadfield_allowmultiple" value="yes" <?php if(esc_attr(get_option('tcap_addfileuploadfield_allowmultiple')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable Multiple Files Upload
						</div>
						<div class="row">
							<label><strong>Maximum File Size</strong></label>
							<br />
							<input type="number" name="tcap_addfileuploadfield_allowmaxsize" value="<?php echo esc_attr(get_option('tcap_addfileuploadfield_allowmaxsize')); ?>" min="1" max="99" />
							<p>Upload file size unit in megabyte (MB)</p>
						</div>
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Add Phone Field</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_addphonefield" value="yes" <?php if(esc_attr(get_option('tcap_addphonefield')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<label><strong>Field Label</strong></label>
							<br />
							<input type="text" name="tcap_addphonefield_label" value="<?php echo esc_attr(get_option('tcap_addphonefield_label')); ?>" />
						</div>
						<div class="row">
							<input type="checkbox" name="tcap_addphonefield_maskenable" value="yes" <?php if(esc_attr(get_option('tcap_addphonefield_maskenable')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable Input Mask
						</div>
						<div class="row">
							<label><strong>Input Mask Pattern</strong></label>
							<br />
							<input type="text" name="tcap_addphonefield_maskpattern" value="<?php echo esc_attr(get_option('tcap_addphonefield_maskpattern')); ?>" />
							<p>Add phone input pattern (i.e. (xxx) xxx-xxxx, xxx-xxx-xxxx  etc.)</p>
						</div>
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row">Notification For User</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_notificationforuserenable" value="yes" <?php if(esc_attr(get_option('tcap_notificationforuserenable')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<label><strong>Email Subject</strong></label>
							<br />
							<input type="text" name="tcap_notificationforusersubject" value="<?php echo esc_attr(get_option('tcap_notificationforusersubject')); ?>" />
						</div>
						<div class="row">
							<label><strong>Email Body Text</strong></label>
							<br />
							<?php
							$tcap_notificationforuserbody_val = get_option('tcap_notificationforuserbody');
							wp_editor($tcap_notificationforuserbody_val, 'tcap_notificationforuserbody', $settings=array('textarea_rows'=>'10'));
							?>
						</div>
					</td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Admin Email Subject</th>
					<td><input type="text" name="tcap_adminemailsubject" value="<?php echo esc_attr(get_option('tcap_adminemailsubject')); ?>" /></td>
				</tr>

				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Redirect URL</th>
					<td>
						<div class="row">
							<input type="checkbox" name="tcap_contactformredirectenable" value="yes" <?php if(esc_attr(get_option('tcap_contactformredirectenable')) == 'yes') { echo 'checked="checked"'; } ?> /> Enable
						</div>
						<div class="row">
							<label><strong>URL</strong></label>
							<br />
							<input type="text" name="tcap_contactformredirecturl" value="<?php echo esc_attr(get_option('tcap_contactformredirecturl')); ?>" />
						</div>
						<div class="row">
							<input type="checkbox" name="tcap_contactformredirectopeninnewtab" value="yes" <?php if(esc_attr(get_option('tcap_contactformredirectopeninnewtab')) == 'yes') { echo 'checked="checked"'; } ?> /> Open redirection in new tab
						</div>
					</td>
				</tr>

			</table>
			
			<hr />
			<h2>Divi Opt-in Form</h2>
			<table class="form-table">
				<tr valign="top" class="form-field">
					<th scope="row" style="width:30%;">Enable reCAPTCHA</th>
					<td>
						<input type="radio" name="tcap_optin" value="no" <?php if(esc_attr(get_option('tcap_optin')) == '' || esc_attr(get_option('tcap_optin')) == 'no') { echo 'checked="checked"'; } ?> /> No
						&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="tcap_optin" value="yes" <?php if(esc_attr(get_option('tcap_optin')) == 'yes') { echo 'checked="checked"'; } ?> /> Yes
					</td>
				</tr>
			</table>
			<hr />
			
			<?php submit_button(); ?>

		</form>
	</div>
	<link rel="stylesheet" href="<?php echo plugins_url('css/backend.css',__FILE__); ?>" />
	<script type="text/javascript" src="<?php echo plugins_url('js/backend.js',__FILE__); ?>" ></script>
	<?php
}

function trumani_plugin_settings()
{
	register_setting('trumani_form_field_list', 'tcap_sitekey');
	register_setting('trumani_form_field_list', 'tcap_secretkey');
	register_setting('trumani_form_field_list', 'tcap_theme');
	register_setting('trumani_form_field_list', 'tcap_invisible');
	register_setting('trumani_form_field_list', 'tcap_captchaalign');

	register_setting('trumani_form_field_list', 'tcap_contactform');
	register_setting('trumani_form_field_list', 'tcap_contactformbtnalign');

	register_setting('trumani_form_field_list', 'tcap_sendto');
	register_setting('trumani_form_field_list', 'tcap_sendto_email');
	register_setting('trumani_form_field_list', 'tcap_sendto_routing');

	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_label');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowext');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowmultiple');
	register_setting('trumani_form_field_list', 'tcap_addfileuploadfield_allowmaxsize');

	register_setting('trumani_form_field_list', 'tcap_addphonefield');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_label');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_maskenable');
	register_setting('trumani_form_field_list', 'tcap_addphonefield_maskpattern');

	register_setting('trumani_form_field_list', 'tcap_notificationforuserenable');
	register_setting('trumani_form_field_list', 'tcap_notificationforusersubject');
	register_setting('trumani_form_field_list', 'tcap_notificationforuserbody');

	register_setting('trumani_form_field_list', 'tcap_adminemailsubject');

	register_setting('trumani_form_field_list', 'tcap_contactformredirectenable');
	register_setting('trumani_form_field_list', 'tcap_contactformredirecturl');
	register_setting('trumani_form_field_list', 'tcap_contactformredirectopeninnewtab');

	register_setting('trumani_form_field_list', 'tcap_optin');
}
?>