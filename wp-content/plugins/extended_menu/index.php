<?php
/*
* Plugin Name: Divi Extended Menu
* Description: Quickly add new style and function to your primary and mobile menus
* Version: 1.0
* Author: Trumani
* Author URI: https://trumani.com/divi-extended-menu-plugin/
*/

require_once ABSPATH . "wp-load.php";
require_once(ABSPATH . 'wp-admin/includes/image.php');
require_once(ABSPATH . 'wp-admin/includes/media.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');

class DS_Extendedmenu {
    function __construct() {
        //add_action( 'admin_init',array( $this, 'admin_functions' ) );
        add_action('customize_register', array( $this,'extended_menu_settings'));

        //add_action( 'wp_head', array( $this,'mytheme_customize_css'));
        add_action( 'wp_head' , array( $this,'header_output') );

        add_action( 'wp_head' , array( $this,'checkboxselect') );

        add_filter('wp_nav_menu_objects', array( $this,'menu_has_children'), 10, 2);

        add_action("init", array($this , "Getsidebar"));
        //add_filter('show_admin_bar', '__return_false');
        //add_action( 'customize_preview_init' , 'ds_extendedmenu' , array( $this,'live_preview') );

        add_action( 'widgets_init', array($this , 'wpdocs_theme_slug_widgets_init') );



        // z($menuID)

        //print_r(get_term_by( 'parent', 18, 'nav_menu' ));die();

         //$primaryNav = wp_get_nav_menu_items($menuID);die();

    }

    
    function wpdocs_theme_slug_widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Extended Sidebar', 'textdomain' ),
            'id'            => 'ds-extended-sidebar',
            'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
            'before_widget' => '<li id="%1$s" class="widget %2$s">',
            'after_widget'  => '</li>',
            'before_title'  => '<h2 class="widgettitle">',
            'after_title'   => '</h2>',
        ) );
    }



    function Getsidebar()
    {
      //get_sidebar();
    }
    function menu_has_children($sorted_menu_items, $args) {
        $parents = array();
        foreach ( $sorted_menu_items as $key => $obj )
                $parents[] = $obj->menu_item_parent;
        foreach ($sorted_menu_items as $key => $obj)
            $sorted_menu_items[$key]->has_children = (in_array($obj->ID, $parents)) ? true : false;
        return $sorted_menu_items;
    }
    function extended_menu_settings($wp_customize)
    {
         //adding section in wordpress customizer   
        $wp_customize->add_section('extended_section', array(
          'title'          => 'Extended Menu Settings',
         ));
        //adding setting for footer text area
        $wp_customize->add_setting('side_menu', array(
         'default'        => '',
         ));
        $wp_customize->add_control('side_menu', array(
         'label'   => 'Display side menu',
          'section' => 'extended_section',
         'type'    => 'checkbox',
        ));
        $wp_customize->add_setting('mobile_child_items_collapsed', array(
         'default'        => '',
         ));
        $wp_customize->add_control('mobile_child_items_collapsed', array(
         'label'   => 'Mobile child items collapsed',
          'section' => 'extended_section',
         'type'    => 'checkbox',
        ));


       


        $wp_customize->add_setting('mega_menu_dividers', array(
         'default'        => '',
         ));
        $wp_customize->add_control('mega_menu_dividers', array(
         'label'   => 'Mega menu column dividers',
          'section' => 'extended_section',
         'type'    => 'checkbox',
        ));

         



        $wp_customize->add_setting('mega_menu_widths', array(
         'default'        => '',
         ));
        $wp_customize->add_control('mega_menu_widths', array(
         'label'   => 'Better mega menu widths',
          'section' => 'extended_section',
         'type'    => 'checkbox',
        ));

        $wp_customize->add_setting('hide_secondary_menu', array(
         'default'        => '',
         ));
        $wp_customize->add_control('hide_secondary_menu', array(
         'label'   => 'Hide secondary menu in mobile',
          'section' => 'extended_section',
         'type'    => 'checkbox',
        ));

        //  $wp_customize->add_setting('menu_no', array(
        //  'default'        => '',
        //  ));
        // $wp_customize->add_control('menu_no', array(
        //  'label'   => 'Mega Menu no',
        //   'section' => 'extended_section',
        //  'type'    => 'number',
        // ));

        

        
        $wp_customize->add_setting( 'primary_menu_hover_effect', array(
          'capability' => 'edit_theme_options',
          'default' => '',
          'sanitize_callback' => '',
        ) );

        $wp_customize->add_control( 'primary_menu_hover_effect', array(
          'type' => 'radio',
          'section' => 'extended_section', // Add a default or your own section
          'label' => 'Primary menu hover effects',
          'description' => '',
          'choices' => array(
            'default' => 'Divi Default',
            'underline'=>'Underline',
            'background'=>'Background color change',
          ),
        ) );
            $wp_customize->add_setting( 'color', array(
              'default'   => '#000000',
              'transport' => 'refresh',
            ) );

            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color', array(
              'section' => 'extended_section',
              'label'   => 'Color',
            ) ) );

            $wp_customize->add_setting( 'background_color', array(
              'default'   => '',
              'transport' => 'refresh',
            ) );

            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color', array(
              'section' => 'extended_section',
              'label'   => 'Background color',
              'settings'=> 'background_color',
            ) ) );

            $wp_customize->add_setting( 'mobile_menu_bar_color', array(
              'default'   => '',
              'transport' => 'refresh',
            ) );

            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_bar_color', array(
              'section' => 'extended_section',
              'label'   => 'Mobile Menu Bar Color',
            ) ) );
            $wp_customize->add_setting( 'mobile_menu_bar_background_color', array(
              'default'   => '',
              'transport' => 'refresh',
            ) );

            $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_bar_background_color', array(
              'section' => 'extended_section',
              'label'   => 'Mobile Menu Icon Background Color',
            ) ) );


            //$wp_customize->get_setting( 'color' )->transport = 'refresh';
      }
      function checkboxselect()
      {
        if(get_theme_mod('side_menu') == 1)
        {
           wp_enqueue_script(
            'extended-menu-script',
             plugin_dir_url( __FILE__ ) . 'js/extended_menu.js',
            array('jquery')
          );
        
        // get_sidebar();
        ?>


        <?php if ( is_active_sidebar( 'ds-extended-sidebar' ) ) : ?>
          <div id="extended_sidebar">
            <?php dynamic_sidebar( 'ds-extended-sidebar' ); ?>
          </div>
        <?php endif; ?>


        <?php
        }

       

        if(get_theme_mod('mobile_child_items_collapsed') == 1)
        {
        ?>
        <style type="text/css">

          #main-header .et_mobile_menu .menu-item-has-children > a { background-color: transparent; position: relative; }
          #main-header .et_mobile_menu .menu-item-has-children > a:after { font-family: 'ETmodules'; text-align: center; speak: none; font-weight: normal; font-variant: normal; text-transform: none; -webkit-font-smoothing: antialiased; position: absolute; }
          #main-header .et_mobile_menu .menu-item-has-children > a:after { font-size: 16px; content: '\4c'; top: 13px; right: 10px; }
          #main-header .et_mobile_menu .menu-item-has-children.visible > a:after { content: '\4d'; }
          #main-header .et_mobile_menu ul.sub-menu { display: none !important; visibility: hidden !important;  transition: all 1.5s ease-in-out;}
          #main-header .et_mobile_menu .visible > ul.sub-menu { display: block !important; visibility: visible !important; }
        </style>

        <script type="text/javascript">
          (function($) {
                
              function setup_collapsible_submenus() {
                  var $menu = $('#mobile_menu'),
                      top_level_link = '#mobile_menu .menu-item-has-children > a';
                       
                  $menu.find('a').each(function() {
                      $(this).off('click');
                        
                      if ( $(this).is(top_level_link) ) {
                          $(this).attr('href', '#');
                      }
                        
                      if ( ! $(this).siblings('.sub-menu').length ) {
                          $(this).on('click', function(event) {
                              $(this).parents('.mobile_nav').trigger('click');
                          });
                      } else {
                          $(this).on('click', function(event) {
                              event.preventDefault();
                              $(this).parent().toggleClass('visible');
                          });
                      }
                  });
              }
                
              $(window).load(function() {
                  setTimeout(function() {
                      setup_collapsible_submenus();
                  }, 700);
              });
           
          })(jQuery);
        </script>
        <?php
        }
        if(get_theme_mod('mega_menu_dividers') == 1)
        {

      ?>

      <style>

          #top-menu li.mega-menu>ul>li {
              border-left : 2px solid rgba(0,0,0,.08) !important;
            
          }

          #top-menu li.mega-menu>ul>li:nth-child(1) {
              border-left : 0 !important;
          }

        </style>



      <?php
    }

      if(get_theme_mod('mega_menu_widths') == 1)
      {

      ?>
      <style>
      
        /*#top-menu li.mega-menu>ul>li
        {
          width: 45% !important;
        }*/
        /*#top-menu li.mega-menu>ul
        {
          width: auto !important;
          
        }*/
        /*.et_fullwidth_nav #top-menu li.mega-menu>ul
        {
          left: 26% !important;
        }*/
      </style>

      <script>

      jQuery(document).ready(function($){


        setTimeout(function(){

          var submenus = $('#top-menu li.mega-menu>ul');
          for (var i = 0; i < submenus.length; i++) {

            var submenu_pos = $(submenus[i]).offset()

            console.log(submenu_pos)

if(i >= 1)
{
  $(submenus[i]).css({ "right" : "0 !important"})
}
else
{
   $(submenus[i]).css({ "right" : "unset !important"})
}
            

            


            if($(submenus[i]).children().size() == 2)
            {
              console.log("width",$("#top-menu").width())
              $(submenus[i]).css({"left": $("#top-menu>li:nth-child(2)").offset().left - 472 + " !important" })

              console.log($(submenus[i]))
              
              var childs = $(submenus[i]).children()
              for (var y = 0; y < childs.length; y++) {
                $(childs[y]).removeClass("width55submenu").addClass("width45submenu")
              }
              $(submenus[i]).removeClass("width55").addClass("width45")
            }
            else if($(submenus[i]).children().size() == 3)
            {
              $(submenus[i]).css({"left": $("#top-menu>li:nth-child(3)").offset().left - 472 + " !important"})
              var childs = $(submenus[i]).children()
              for (var y = 0; y < childs.length; y++) {
                $(childs[y]).removeClass("width45submenu").addClass("width55submenu")
              }
              $(submenus[i]).removeClass("width45").addClass("width55")
            }
          }



  },1000)
        var submenus = $("li.mega-menu.menu-item>ul.sub-menu");

        for (var i = 0; i < submenus.length; i++) {
          console.log($(submenus[i]).find("li"))
        }

        

         //$("ul#top-menu>li").addClass('mega-menu');

        // var parent_menu = $("#top-menu").find(".mega-menu");

        // for (var i = 0; i < parent_menu.length; i++) {
        //   console.log(parent_menu[i])
        //   var submenu = $(parent_menu[i]+".sub-menu>li")

        //   console.log("submenu length "+i +" "+submenu.length)
        // }

        



      })



      </script>




      <?php

      }
      if(get_theme_mod('hide_secondary_menu') == 1)
      {

        $locations = get_nav_menu_locations();

        $menus = wp_get_nav_menu_items($locations['secondary-menu']);

       



      ?>
     
        <style type="text/css">
        @media only screen and (max-width: 980px)  {
        <?php  foreach ($menus as $key => $menu){ ?>

          .menu-item-<?php echo $menu->ID ?>
          {
            display: none !important;

          }
        <?php 
        }
        ?>
      }
        </style>
      <?php 
    }
      // if(isset(get_theme_mod('mobile_menu_bar_color')))
      // {
      ?>
        <style type="text/css">
          .mobile_menu_bar:before
          {
            color : <?php echo get_theme_mod('mobile_menu_bar_color'); ?> !important;
            background-color : <?php echo get_theme_mod('mobile_menu_bar_background_color'); ?> !important;
            
          }
         #top-menu li {
            display: inline-block !important;
            /* padding-right: 22px; */
            font-size: 14px !important;
            margin-right: 15px !important;
            padding: 10px !important;
        }


        </style>

        

      
        
      <?php 
      //}

      

  }
  function header_output() {
wp_enqueue_style( 'extended-menu-style',  plugin_dir_url( __FILE__ ) . 'css/extended_menu.css');

      ?>
      <style type="text/css"> 
           <?php 

           if(get_theme_mod('primary_menu_hover_effect') == 'background')
           {
              //self::generate_css('#top-menu li:hover', 'background-color', 'color');
          ?>
          <style type="text/css">

          
              #top-menu li
              {
                padding: 10px !important;
              }

              #top-menu>li:hover{

                background-color : <?php echo get_theme_mod('color'); ?> ;


              
              }
          </style>
          <?php
           }
           elseif(get_theme_mod('primary_menu_hover_effect') == 'underline')
           {
            
          ?>
            <style type="text/css">
              #top-menu .current-menu-item a::before,
              #top-menu .current_page_item a::before {
               content: "";
               position: absolute;
               z-index: 2;
               left: 0;
               right: 0;
              }
              #top-menu li a:before {
               content: "";
               position: absolute;
               z-index: -2;
               left: 0;
               right: 100%;
               bottom: 50%;
               background: <?php echo get_theme_mod('color'); ?>; /*** COLOR OF THE LINE ***/
               height: 3px; /*** THICKNESS OF THE LINE ***/
               -webkit-transition-property: right;
               transition-property: right;
               -webkit-transition-duration: 0.3s;
               transition-duration: 0.3s;
               -webkit-transition-timing-function: ease-out;
               transition-timing-function: ease-out;
              }
              #top-menu li a:hover {
               opacity: 1 !important;
              }
              #top-menu li a:hover:before {
               right: 0;
              }
              #top-menu li li a:before {
               bottom: 10%;
              }
            </style>
          <?php

           }

            


           ?>
      </style> 
      <?php
   }
   function mytheme_customize_css()
    {
        ?>
             <style type="text/css">
                 li.current_page_item { background-color: <?php echo get_theme_mod('color', '#000000'); ?>; }
             </style>
        <?php
    }
   public static function live_preview() {
      wp_enqueue_script( 
           'ds_extendedmenu', // Give the script a unique ID
           get_template_directory_uri() . '/js/theme-customizer.js', // Define the path to the JS file
           array(  'jquery', 'customize-preview' ), // Define dependencies
           '', // Define a version (optional) 
           true // Specify whether to put in footer (leave this true)
      );
   }
    public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
      $return = '';
      $mod = get_theme_mod($mod_name);
      if ( ! empty( $mod ) ) {
         $return = sprintf('%s { %s:%s; }',
            $selector,
            $style,
            $prefix.$mod.$postfix
         );
         if ( $echo ) {
            echo $return;
         }
      }
      return $return;
    }
      

}

new DS_Extendedmenu();

// if ( function_exists('register_sidebar') )
//   register_sidebar(array(
//     'name' => 'Extended sidebar',
//     'before_widget' => '<div class = "widgetizedArea">',
//     'after_widget' => '</div>',
//     'before_title' => '<h3>',
//     'after_title' => '</h3>',
//   )
// );

?>