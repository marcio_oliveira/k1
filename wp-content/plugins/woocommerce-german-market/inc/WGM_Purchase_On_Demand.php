<?php

/**
 * Class WGM_Purchase_On_Demand
 *
 * German Market Gateway Puchase on Demand
 *
 * @author MarketPress
 */
class WGM_Purchase_On_Demand {

	/**
	 * @var WGM_Purchase_On_Demand
	 * @since v3.2
	 */
	private static $instance = null;
	
	/**
	* Singletone get_instance
	*
	* @static
	* @return WGM_Purchase_On_Demand
	*/
	public static function get_instance() {
		if ( self::$instance == NULL) {
			self::$instance = new WGM_Purchase_On_Demand();	
		}
		return self::$instance;
	}

	/**
	* Singletone constructor
	*
	* @access private
	*/
	private function __construct() {

		if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
			return;
		}
		
		require_once dirname( Woocommerce_German_Market::$plugin_filename ) . '/gateways/WGM_Gateway_Purchase_On_Account.php';
		
		$poa_settings = get_option( 'woocommerce_german_market_purchase_on_account_settings' );
		$costs = $poa_settings[ 'woocommerce_german_market_purchase_on_account_fee' ];
		if ( floatval( str_replace( ',', '.', $costs ) ) > 0.0 ) {
			WGM_Gateways::set_gateway_fee( 'german_market_purchase_on_account' , $poa_settings[ 'woocommerce_german_market_purchase_on_account_fee' ] );
		}

		add_filter( 'woocommerce_payment_gateways', array( $this, 'german_market_add_purchase_on_account' ) );

		do_action( 'wgm_after_purchase_on_demand_init', $this );

	}

	/**
	* Add Gateway
	*
	* @since GM 3.2
	* @wp-hook woocommerce_payment_gateways
	* @param Array $gateway
	* @return Array
	**/
	public function german_market_add_purchase_on_account( $gateways ) {
		$gateways[] = 'WGM_Gateway_Purchase_On_Account';
		return $gateways;
	}


}
