<?php
/**
 * Feature Name: Helper
 * Descriptions: Here are some helper functions we need
 * Version:      1.0
 * Author:       MarketPress
 * Author URI:   https://marketpress.com
 * Licence:      GPLv3
 */

/**
 * getting the Script and Style suffix for Kiel-Theme
 * Adds a conditional ".min" suffix to the file name when WP_DEBUG is NOT set to TRUE.
 *
 * @return	string
 */
function wcppufv_get_script_suffix() {

	$script_debug = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG;
	$suffix = $script_debug ? '' : '.min';

	return $suffix;
}

/**
 * Gets the specific asset directory url
 *
 * @param	string $path the relative path to the wanted subdirectory. If
 *				no path is selected, the root asset directory will be returned
 * @return	string the url of the wcppufv asset directory
 */
function wcppufv_get_asset_directory_url( $path = '' ) {

	// set base url
	$wcppufv_assets_url = WCPPUFV_PLUGIN_URL . 'assets/';
	if ( $path != '' )
		$wcppufv_assets_url .= $path . '/';
	return $wcppufv_assets_url;
}

/**
 * Gets the specific asset directory path
 *
 * @param	string $path the relative path to the wanted subdirectory. If
 *				no path is selected, the root asset directory will be returned
 * @return	string the url of the wcppufv asset directory
 */
function wcppufv_get_asset_directory( $path = '' ) {

	// set base url
	$wcppufv_assets = WCPPUFV_PLUGIN_PATH . 'assets/';
	if ( $path != '' )
		$wcppufv_assets .= $path . '/';
	return $wcppufv_assets;
}

/**
 * Make a select field for scale_units
 * 
 * @param	array $field
 * @return	string html
 */
function wcppufv_select_scale_units( $post_id, $field ) {
	global $woocommerce;

	if ( ! isset( $field[ 'class' ] ) )
		$field[ 'class' ] = 'select short';

	if ( ! isset( $field[ 'value' ] ) )
		$field[ 'value' ] = get_post_meta( $post_id, $field[ 'name' ], TRUE );

	$default_product_attributes = WGM_Defaults::get_default_product_attributes();

	$attribute_taxonomy_name = wc_attribute_taxonomy_name( $default_product_attributes[ 0 ][ 'attribute_name' ] );

	$string = '<select name="' . $field[ 'id' ] . '">';

	$terms = get_terms( $attribute_taxonomy_name, 'orderby=name&hide_empty=0' );

	foreach ( $terms as  $value ) {
		
		if ( is_object( $value ) && isset( $value->name ) && isset( $value->description ) ) {

			$string .= '<option value="'. $value->name .'" ';
			$string .= selected( $field[ 'value' ], $value->name, FALSE );
			$string .=  '>'. $value->description . '</option>';

		}
		
	}

	$string .= '</select>';
	return $string;
}

/**
 * Retrives price per unit data
 * 
 * @param	int $variation_id
 * @param	object $product
 * @return	array
 */
function wcppufv_get_price_per_unit_data( $variation_id, $product ) {

	if ( $product->is_on_sale() ) {
		$rtn[ 'price_per_unit' ] = str_replace( ',', '.', get_post_meta( $variation_id, '_v_sale_price_per_unit', TRUE ) );
		$rtn[ 'unit' ] = get_post_meta( $variation_id, '_v_unit_sale_price_per_unit', TRUE );
		$rtn[ 'mult' ] = get_post_meta( $variation_id, '_v_unit_sale_price_per_unit_mult', TRUE );
	} else {
		$rtn[ 'price_per_unit' ] = str_replace( ',', '.', get_post_meta( $variation_id, '_v_regular_price_per_unit', TRUE ) );
		$rtn[ 'unit' ] = get_post_meta( $variation_id, '_v_unit_regular_price_per_unit', TRUE );
		$rtn[ 'mult' ] = get_post_meta( $variation_id, '_v_unit_regular_price_per_unit_mult', TRUE );
	}

	return $rtn;
}
