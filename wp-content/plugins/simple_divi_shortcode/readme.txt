/*---- Simple Divi Shortcode ----*/
Contributors: Fabrice ESQUIROL
Tags: Divi, Divi theme, Divi Modules, Divi Library, Divi Sections, Divi Builder, Elegant Themes
Donate link: https://www.creaweb2b.com
Requires at least: 4.0
Tested up to: 4.9.7
Requires PHP: 5.6 or higher (tested up to 7.2.1)
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows you to use a shortcode to insert a section, a module or a layout from the DIVI Library inside another module content or inside a template using a shortcode 
[showmodule id="xxx"] where xxx is the ID of the section, module or layout inside the DIVI Library.

== Description ==
Using this tool you will be able to embed any Divi Library item inside another module content or inside a php template by using a simple shortcode.

You just need to build a layout, section or module inside the Divi library.

The shortcode can be copied from Divi Library screen (Shortcode column) or from the metabox inside the Divi builder.

Once you get shortcode copied (it looks like [showmodule id="866"]), you can insert it inside another module content or inside a php template.

I made a tutorial explaining how to use it : "DIVI Module inside module v2" available at the following URL :
https://www.creaweb2b.com/en/divi-module-inside-module-v2/

French version : 
https://www.creaweb2b.com/module-section-divi-module-v2/

== Installation ==
Install the plugin by uploading zip file, directly from your plugins page, or manually upload zip file to your server to the /wp-content/plugins/ folder 
(then you need to unzip it)

Activate the plugin through the "Plugins" menu in WordPress.

== Changelog ==
- 0.9 - Initial release. Free version available from Wordpress Repository
- 1.0 - Premium version adding Shortcode Colmumn inside Divi Library and Shortcode metabox inside Divi Builder
- 1.1 - Premium version 1.0 + add direct copy from column and metabox
- 1.2 - Premium version 1.1 + Updated deprecated extract attribute method - Modification de la méthode d'extraction des attributs (l'ancienne étant dépréciée)