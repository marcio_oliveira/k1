��          T      �       �      �      �      �   g   �      [     r  (  �     �     �      �  �   �     t     �                                        Bernhard Kau Language Fallback Language Fallback Settings Set a language as a fallback for the chosen language (e.g. "Deutsch" as a fallback for "Deutsch (Sie)") Site Fallback Language http://kau-boys.com PO-Revision-Date: 2016-04-08 14:31:05+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.3.0-alpha
Language: de
Project-Id-Version: Plugins - Language Fallback - Stable (latest release)
 Bernhard Kau Language Fallback Einstellungen für Ersatzsprache Einstellen einer Ersatzsprache, falls die eingestellte Sprache nicht verfügbar ist (z.B. "Deutsch" als Ersatz für "Deutsch (Sie)". Ersatzsprache der Website http://kau-boys.de 